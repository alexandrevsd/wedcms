'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Voucher extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    Voucher.init({
        voucher_code: {type: DataTypes.STRING(100), primaryKey: true},
        credits: DataTypes.INTEGER,
        expiry_date: DataTypes.DATE,
        is_single_use: DataTypes.BOOLEAN,
    }, {
        sequelize,
        timestamps: false,
        modelName: 'Voucher'
    });
    return Voucher;
};