'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedHelpMessage extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedHelpMessage.init({
        username: DataTypes.STRING,
        email: DataTypes.STRING,
        message: DataTypes.STRING,
        date: DataTypes.DATE,
        ip: DataTypes.STRING
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_help_messages',
        modelName: 'WedHelpMessage'
    });
    return WedHelpMessage;
};