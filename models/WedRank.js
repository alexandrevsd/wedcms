'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedRank extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedRank.init({
        id: {type: DataTypes.INTEGER, primaryKey: true},
        name: DataTypes.STRING
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_ranks',
        modelName: 'WedRank'
    });
    return WedRank;
};