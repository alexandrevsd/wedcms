'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedAnnouncement extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedAnnouncement.init({
        user_id: DataTypes.INTEGER,
        title: DataTypes.STRING,
        content: DataTypes.STRING,
        image: DataTypes.STRING,
        button1_link: DataTypes.STRING,
        button1_text: DataTypes.STRING,
        button2_link: DataTypes.STRING,
        button2_text: DataTypes.STRING,
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_announcements',
        modelName: 'WedAnnouncement'
    });
    return WedAnnouncement;
};