'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedPaypalPayment extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedPaypalPayment.init({
        payment_id: DataTypes.STRING,
        paypal_item_id: DataTypes.INTEGER
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_paypal_payments',
        modelName: 'WedPaypalPayment'
    });
    return WedPaypalPayment;
};