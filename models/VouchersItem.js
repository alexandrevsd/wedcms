'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class VouchersItem extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    VouchersItem.init({
        voucher_code: {type: DataTypes.STRING(100), primaryKey: true},
        catalogue_sale_code: DataTypes.STRING(100),
    }, {
        sequelize,
        timestamps: false,
        tableName: 'vouchers_items',
        modelName: 'VouchersItem'
    });
    return VouchersItem;
};