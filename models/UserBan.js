'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class UserBan extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    UserBan.init({
        ban_type: DataTypes.STRING,
        banned_value: DataTypes.INTEGER,
        message: DataTypes.STRING,
        banned_until: DataTypes.INTEGER
    }, {
        sequelize,
        tableName: 'users_bans',
        timestamps: false,
        modelName: 'UserBan'
    });
    UserBan.removeAttribute('id');
    return UserBan;
};