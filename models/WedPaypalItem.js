'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedPaypalItem extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedPaypalItem.init({
        price: DataTypes.INTEGER,
        currency: DataTypes.STRING,
        paypal_currency: DataTypes.STRING,
        credits: DataTypes.INTEGER,
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_paypal_items',
        modelName: 'WedPaypalItem'
    });
    return WedPaypalItem;
};