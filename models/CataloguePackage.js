'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class CataloguePackage extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    CataloguePackage.init({
        id: { type: DataTypes.INTEGER, primaryKey: true },
        salecode: DataTypes.STRING,
        definition_id: DataTypes.INTEGER,
        special_sprite_id: DataTypes.INTEGER,
        amount: DataTypes.INTEGER,
    }, {
        sequelize,
        timestamps: false,
        tableName: 'catalogue_packages',
        modelName: 'CataloguePackage'
    });
    return CataloguePackage;
};