'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedNews extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedNews.init({
        user_id: DataTypes.INTEGER,
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        content: DataTypes.TEXT,
        date: DataTypes.DATE,
        author_username: DataTypes.STRING,
        author_rank: DataTypes.INTEGER
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_news',
        modelName: 'WedNews'
    });
    return WedNews;
};