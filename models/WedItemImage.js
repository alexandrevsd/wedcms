'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedItemImage extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedItemImage.init({
        id: {type: DataTypes.INTEGER, primaryKey: true},
        item_definition_id: DataTypes.INTEGER,
        little_image: DataTypes.STRING
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_item_images',
        modelName: 'WedItemImage'
    });
    return WedItemImage;
};