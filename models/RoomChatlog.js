'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class RoomChatlog extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    RoomChatlog.init({
        user_id: DataTypes.INTEGER,
        room_id: DataTypes.INTEGER,
        timestamp: DataTypes.INTEGER,
        chat_type: DataTypes.INTEGER,
        message: DataTypes.STRING
    }, {
        sequelize,
        timestamps: false,
        tableName: 'room_chatlogs',
        modelName: 'RoomChatlog'
    });
    RoomChatlog.removeAttribute('id');
    return RoomChatlog;
};