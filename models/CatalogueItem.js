'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class CatalogueItem extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    CatalogueItem.init({
        sale_code: DataTypes.STRING,
        page_id: DataTypes.INTEGER,
        order_id: DataTypes.INTEGER,
        price: DataTypes.INTEGER,
        is_hidden: DataTypes.BOOLEAN,
        amount: DataTypes.INTEGER,
        definition_id: DataTypes.INTEGER,
        item_specialspriteid: DataTypes.INTEGER,
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        is_package: DataTypes.BOOLEAN,
        package_name: DataTypes.STRING,
        package_description: DataTypes.STRING,
    }, {
        sequelize,
        timestamps: false,
        tableName: 'catalogue_items',
        modelName: 'CatalogueItem'
    });
    return CatalogueItem;
};