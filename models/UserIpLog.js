'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class UserIpLog extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    UserIpLog.init({
        user_id: {type: DataTypes.INTEGER, primaryKey: true},
        ip_address: DataTypes.STRING,
        created_at: DataTypes.DATE
    }, {
        sequelize,
        tableName: 'users_ip_logs',
        timestamps: false,
        modelName: 'UserIpLog'
    });
    return UserIpLog;
};