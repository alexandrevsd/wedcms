'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class ItemDefinition extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    ItemDefinition.init({
        sprite: DataTypes.STRING,
        sprite_id: DataTypes.INTEGER,
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        colour: DataTypes.STRING(100),
        length: DataTypes.STRING,
        width: DataTypes.STRING,
        top_height: DataTypes.NUMBER,
        max_status: DataTypes.STRING(11),
        behaviour: DataTypes.STRING,
        interactor: DataTypes.STRING,
        is_tradable: DataTypes.BOOLEAN,
        is_recyclable: DataTypes.BOOLEAN,
        drink_ids: DataTypes.TEXT
    }, {
        sequelize,
        timestamps: false,
        tableName: 'items_definitions',
        modelName: 'ItemDefinition'
    });
    return ItemDefinition;
};