'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Item extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    Item.init({
        order_id: DataTypes.INTEGER,
        user_id: DataTypes.INTEGER,
        room_id: DataTypes.INTEGER,
        definition_id: DataTypes.INTEGER,
        x: DataTypes.STRING,
        y: DataTypes.STRING,
        z: DataTypes.STRING,
        wall_position: DataTypes.STRING,
        rotation: DataTypes.INTEGER,
        custom_data: DataTypes.STRING,
        is_hidden: DataTypes.BOOLEAN,
        created_at: DataTypes.DATE,
        updated_at: DataTypes.DATE
    }, {
        sequelize,
        timestamps: false,
        tableName: 'items',
        modelName: 'Item'
    });
    return Item;
};