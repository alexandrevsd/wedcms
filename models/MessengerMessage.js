'use strict';
const { number, string } = require('handlebars-helpers/lib');
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class MessengerMessage extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    MessengerMessage.init({
        id: { type: DataTypes.INTEGER, primaryKey: true },
        receiver_id: DataTypes.INTEGER,
        sender_id: DataTypes.INTEGER,
        unread: DataTypes.STRING,
        body: DataTypes.STRING,
        date: DataTypes.INTEGER
    }, {
        sequelize,
        timestamps: false,
        tableName: 'messenger_messages',
        modelName: 'MessengerMessage'
    });
    return MessengerMessage;
};