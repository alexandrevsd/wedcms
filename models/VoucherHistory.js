'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class VoucherHistory extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    VoucherHistory.init({
        voucher_code: {type: DataTypes.STRING(100), primaryKey: true},
        user_id: DataTypes.INTEGER,
        used_at: DataTypes.DATE,
        credits_redeemed: DataTypes.INTEGER,
        items_redeemed: DataTypes.STRING,
    }, {
        sequelize,
        timestamps: false,
        tableName: 'vouchers_history',
        modelName: 'VoucherHistory'
    });
    return VoucherHistory;
};