'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class UserBadge extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            //
        }
    };
    UserBadge.init({
        user_id: DataTypes.INTEGER,
        badge: DataTypes.STRING
    }, {
        sequelize,
        tableName: 'users_badges',
        timestamps: false,
        modelName: 'UserBadge'
    });
    UserBadge.removeAttribute('id');
    return UserBadge;
};