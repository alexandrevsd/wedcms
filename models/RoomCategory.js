'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class RoomCategory extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    RoomCategory.init({
        id: { type: DataTypes.INTEGER, primaryKey: true },
        order_id: DataTypes.INTEGER,
        parent_id: DataTypes.INTEGER,
        isnode: DataTypes.INTEGER,
        name: DataTypes.STRING,
        public_spaces: DataTypes.INTEGER,
        allow_trading: DataTypes.INTEGER,
        minrole_access: DataTypes.INTEGER,
        minrole_setflatcat: DataTypes.INTEGER
    }, {
        sequelize,
        timestamps: false,
        tableName: 'rooms_categories',
        modelName: 'RoomCategory'
    });
    return RoomCategory;
};