'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class RareCycle extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    RareCycle.init({
        sale_code: {type: DataTypes.STRING, primaryKey: true},
        reuse_time: DataTypes.DATE,
    }, {
        sequelize,
        timestamps: false,
        tableName: 'rare_cycle',
        modelName: 'RareCycle'
    });
    return RareCycle;
};