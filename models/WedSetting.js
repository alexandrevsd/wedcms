'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class WedSetting extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    WedSetting.init({
        setting: {type: DataTypes.STRING, primaryKey: true},
        value: DataTypes.STRING
    }, {
        sequelize,
        timestamps: false,
        tableName: 'wed_settings',
        modelName: 'WedSetting'
    });
    return WedSetting;
};