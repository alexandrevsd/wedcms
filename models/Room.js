'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Room extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    Room.init({
        owner_id: DataTypes.STRING(11),
        category: DataTypes.INTEGER,
        name: DataTypes.STRING,
        description: DataTypes.STRING,
        model: DataTypes.STRING,
        ccts: DataTypes.STRING,
        wallpaper: DataTypes.INTEGER(4),
        floor: DataTypes.INTEGER(4),
        showname: DataTypes.BOOLEAN,
        superusers: DataTypes.BOOLEAN,
        accesstype: DataTypes.INTEGER(3),
        password: DataTypes.STRING,
        visitors_now: DataTypes.INTEGER,
        visitors_max: DataTypes.INTEGER,
        rating: DataTypes.INTEGER
    }, {
        sequelize,
        timestamps: false,
        tableName: 'rooms',
        modelName: 'Room'
    });
    return Room;
};