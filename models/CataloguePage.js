'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class CataloguePage extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };
    CataloguePage.init({
        id: { type: DataTypes.INTEGER, primaryKey: true },
        order_id: DataTypes.INTEGER,
        min_role: DataTypes.INTEGER,
        index_visible: DataTypes.BOOLEAN,
        is_club_only: DataTypes.BOOLEAN,
        name_index: DataTypes.STRING,
        link_list: DataTypes.STRING,
        name: DataTypes.STRING,
        layout: DataTypes.STRING,
        image_headline: DataTypes.STRING,
        image_teasers: DataTypes.STRING,
        body: DataTypes.STRING,
        label_pick: DataTypes.STRING,
        label_extra_s: DataTypes.STRING,
        label_extra_t: DataTypes.STRING
    }, {
        sequelize,
        timestamps: false,
        tableName: 'catalogue_pages',
        modelName: 'CataloguePage'
    });
    return CataloguePage;
};