'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class User extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            User.hasMany(models.UserBadge, {
                foreignKey: 'user_id'
            })
        }
    };
    User.init({
        username: DataTypes.STRING,
        password: DataTypes.STRING,
        figure: DataTypes.STRING,
        pool_figure: DataTypes.STRING,
        sex: DataTypes.CHAR(1),
        motto: DataTypes.STRING(100),
        credits: DataTypes.INTEGER,
        tickets: DataTypes.INTEGER,
        film: DataTypes.INTEGER,
        rank: DataTypes.INTEGER(1),
        console_motto: DataTypes.STRING(100),
        last_online: DataTypes.INTEGER,
        created_at: DataTypes.INTEGER,
        updated_at: DataTypes.INTEGER,
        sso_ticket: DataTypes.STRING,
        club_subscribed: DataTypes.INTEGER,
        club_expiration: DataTypes.INTEGER,
        club_gift_due: DataTypes.INTEGER,
        badge: DataTypes.CHAR(3),
        badge_active: DataTypes.BOOLEAN,
        allow_stalking: DataTypes.BOOLEAN,
        allow_friend_requests: DataTypes.BOOLEAN,
        sound_enabled: DataTypes.BOOLEAN,
        tutorial_finished: DataTypes.BOOLEAN,
        battleball_points: DataTypes.INTEGER,
        snowstorm_points: DataTypes.INTEGER
    }, {
        sequelize,
        timestamps: false,
        tableName: 'users',
        modelName: 'User'
    });
    return User;
};