const Security = require("../services/Security");
const KeplerUsers = require("../services/kepler/KeplerUsers");
const KeplerRare = require("../services/kepler/KeplerRare");
const WedPaypalItems = require("../services/wed/WedPaypalItems");
const Paypal = require("../services/Paypal");
const KeplerSettings = require('../services/kepler/KeplerSettings');
const Texts = require("../services/Texts");
const ExternalVarsGenerator = require("../services/ExternalVarsGenerator");

module.exports.getExternalVars = async (req, res) => {
    const valuesToChange = new Map();
    // If we got view in URL
    if(req.query.view) {
        // Change view in external vars
        valuesToChange.set('view', req.query.view);
    }
    // Send the external vars file edited
    res.send(ExternalVarsGenerator.generateFile(valuesToChange));
}

module.exports.getHabbosConnected = async (req, res, app) => {
    const habboText = Texts.get(app.text, 'main')['global']['logged'];
    // Get setting users online from db
    const onlineUsers = await KeplerSettings.getSetting('players.online');
    // Set text for 1 or many users
    const connectedText = onlineUsers === '1' ? ' ' + habboText.habbo : ' ' + habboText.habbos;
    const text = onlineUsers + connectedText;
    res.json({text})
}

module.exports.getSubscribeClub = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        // If months sent are not 1, 3 or 6
        if (!req.query || !req.query.months || (req.query.months !== '1' && req.query.months !== '3' && req.query.months !== '6')) {
            res.redirect('/club');
        } else {
            // If months are 1, 3 or 6

            // Set the cost of each month
            let cost = 25;
            if (req.query.months === '3') {
                cost = 60;
            } else if (req.query.months === '6') {
                cost = 105;
            }
            // Get user infos
            const infos = await KeplerUsers.getInfos(req.session.user.id);
            // If user have enough credits
            if (infos.credits >= cost) {
                // Set new user credits amount
                await KeplerUsers.setCredits(req.session.user.id, infos.credits - cost);

                // Duration to set
                const duration = 60 * 60 * 24 * 31 * req.query.months;

                // Set new user club duration
                if (infos.hcDaysLeft > 0) {
                    await KeplerUsers.addClubTime(req.session.user.id, duration);
                } else {
                    await KeplerUsers.setClubTime(req.session.user.id, duration);
                }

                res.redirect('/club');
            } else {
                res.redirect('/club');
            }
        }
    })
}

module.exports.getBuyRare = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        // Get actual rare infos
        const rare = await KeplerRare.getActualRare();
        // Get user infos
        const infos = await KeplerUsers.getInfos(req.session.user.id);
        // If the user have enough credits
        if (infos.credits >= rare.price) {
            // Remove credits from user
            await KeplerUsers.setCredits(req.session.user.id, infos.credits - rare.price);
            // Give rare to user
            await KeplerUsers.giveItems(req.session.user.id, [rare.sale_code]);
        }
        res.redirect('/');
    })
}

module.exports.getBuyCredits = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        const paypalItem = await WedPaypalItems.get(req.params.id);
        // If paypal Item found in db
        if (paypalItem) {
            // Creates paypal payment
            await Paypal.createPayment(paypalItem, (redirection) => {
                // Redirect to paypal payment created
                res.redirect(redirection);
            });
        } else {
            // If paypal Item not found, redirect to paypal payment page
            res.redirect('/credits/paypal');
        }
    })
}