const Security = require("../../services/Security");
const KeplerUsers = require("../../services/kepler/KeplerUsers");
const KeplerItems = require("../../services/kepler/KeplerItems");
const KeplerRooms = require("../../services/kepler/KeplerRooms");
const KeplerUserIpLogs = require("../../services/kepler/KeplerUserIpLogs");
const argon2 = require('argon2');
const KeplerUserBadges = require("../../services/kepler/KeplerUserBadges");
const KeplerMessengerMessages = require("../../services/kepler/KeplerMessengerMessages");


module.exports.getBadges = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const path = require('path');
        const fs = require('fs');

        const directoryPath = path.join(__dirname, '../../ressources/public/v14/c_images/badges/');
        const files = fs.readdirSync(directoryPath);
        const badges = [];
        files.forEach(function(file) {
            if (file.includes('.gif') || file.includes('.GIF')) {
                const badgeName = file.slice(0, -4);
                badges.push(badgeName);
            }
        });

        const userId = req.query.id;
        const userBadges = await KeplerUserBadges.get(userId);

        res.json({
            response: 200,
            userBadges,
            badges
        })
    })
}


module.exports.getConsole = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const userId = req.query.id;

        const messages = await KeplerMessengerMessages.getAll(userId);
        const users = new Map();

        for (let i = 0; i < messages.length; i++) {
            if (!users.has(messages[i].sender_id)) {
                const user = await KeplerUsers.get(messages[i].sender_id);
                users.set(messages[i].sender_id, user);
            }
        }

        res.json({
            response: 200,
            users
        })
    })
}

module.exports.postBadges = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const { userId, badges } = req.body;
        await KeplerUsers.setBadges(userId, badges);
        res.json({
            response: 200
        })
    })
}

module.exports.postUser = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const userFromReq = req.body.user;
        if (userFromReq.id) {
            const userExists = await KeplerUsers.exists(userFromReq.username);
            if (userExists) {
                if (userFromReq.rank <= req.session.user.rank || req.session.user.rank === 7) {
                    await KeplerUsers.edit(userFromReq);
                } else {
                    res.json({
                        response: 400,
                        error: 'notAllowed'
                    });
                    return;
                }
            } else {
                res.json({
                    response: 400,
                    error: 'userDoesntExists'
                });
                return;
            }
        } else {
            const userExists = await KeplerUsers.exists(userFromReq.username);
            if (!userExists) {
                if (userFromReq.password !== '') {
                    if (userFromReq.username !== '') {
                        if (userFromReq.rank <= req.session.user.rank || req.session.user.rank === 7) {
                            const hashedPassword = await argon2.hash(userFromReq.password);
                            await KeplerUsers.create({
                                username: userFromReq.username,
                                password: hashedPassword,
                                rank: userFromReq.rank,
                                pool_figure: '',
                                badge: ''
                            });
                        } else {
                            res.json({
                                response: 400,
                                error: 'notAllowed'
                            });
                            return;
                        }
                    } else {
                        res.json({
                            response: 400,
                            error: 'userUsernameEmpty'
                        });
                        return;
                    }
                } else {
                    res.json({
                        response: 400,
                        error: 'userPasswordEmpty'
                    });
                    return;
                }
            } else {
                res.json({
                    response: 400,
                    error: 'userAlreadyExists'
                });
                return;
            }
        }
        res.json({
            response: 200
        })
    })
}

module.exports.test = async(req, res) => {
    await KeplerItems.giveItem(8, 1438);
    res.json({
        response: 200
    })
}

module.exports.ban = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const { id, message, length } = req.body;
        await KeplerUsers.ban(id, message, length);
        res.json({
            response: 200
        })
    })
}

module.exports.deleteUser = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.query.id;
        const user = await KeplerUsers.get(id);
        if (user.id !== req.session.user.id && (user.rank < req.session.user.rank || req.session.user.rank === 7)) {
            await KeplerUsers.delete(id);
            res.json({
                response: 200
            })
        } else {
            res.json({
                response: 500
            })
        }
    })
}

module.exports.getVouchersList = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.query.id;
        const vouchersList = await KeplerUsers.getVouchersList(id);
        res.json({
            response: 200,
            vouchersList
        })
    })
}

module.exports.getIpLogs = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.query.id;
        const ipLogs = await KeplerUserIpLogs.getList(id);
        res.json({
            response: 200,
            ipLogs
        })
    })
}

module.exports.postUnban = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.body.id;
        await KeplerUsers.unban(id);
        res.json({
            response: 200
        })
    })
}

module.exports.getBansList = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.query.id;
        const bansList = await KeplerUsers.getBansList(id);
        res.json({
            response: 200,
            bansList
        })
    })
}

module.exports.getUsers = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const limit = 20;
        const reqPage = req.query.page || 1;
        const { users, page, next, previous, maxPage } = await KeplerUsers.getList(limit, reqPage);
        res.json({
            response: 200,
            users,
            page,
            next,
            previous,
            maxPage
        })
    })
}

module.exports.deleteUserHc = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.query.id;
        await KeplerUsers.deleteHc(id);
        res.json({
            response: 200
        })
    })
}

module.exports.getUser = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const user = await KeplerUsers.get(req.query.id);
        console.log(user);
        res.json({
            response: 200,
            user
        })
    })
}

/*module.exports.postUser = async (req, res) => {
    Security.checkLoggedFromAPI(req, res, async (req, res) => {
        const newUser = req.body.user;
        KeplerUsers.setSex(newUser.id, newUser.sex);
        await KeplerUsers.setCredits(newUser.id, newUser.credits);
        KeplerUsers.setUsername(newUser.id, newUser.username);
        if(newUser.password !== '' && newUser.password !== undefined) {
            KeplerUsers.setPassword(newUser.id, newUser.password);
        }
        res.json({
            response: 200
        })
    })
}*/

module.exports.getHand = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const userId = req.query.id;

        const limit = 20;

        let page = parseInt(req.query.page) || 1;
        const itemsCount = await KeplerItems.countHandItems(userId);
        const maxPage = Math.ceil(itemsCount / limit);
        let offset = (page - 1) * limit;
        if (offset >= itemsCount) {
            page = Math.ceil(itemsCount / limit);
            offset = (page - 1) * limit;
        }
        const next = (offset + limit) < itemsCount;
        const previous = offset > 0;

        const { items, itemDefinitions, itemImages } = await KeplerItems.getHand(offset, limit, userId);

        res.json({
            response: 200,
            page,
            items,
            itemDefinitions,
            itemImages,
            next,
            previous,
            maxPage
        })
    })
}

module.exports.getRooms = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {

        const limit = 20;

        const userId = req.query.id;
        let page = parseInt(req.query.page) || 1;
        const roomsCount = await KeplerRooms.countRooms(userId);
        const maxPage = Math.ceil(roomsCount / limit);
        let offset = (page - 1) * limit;
        if (offset >= roomsCount) {
            page = Math.ceil(roomsCount / limit);
            offset = (page - 1) * limit;
        }
        const next = (offset + limit) < roomsCount;
        const previous = offset > 0;

        const rooms = roomsCount > 0 ? await KeplerRooms.getRooms(offset, limit, userId) : [];

        res.json({
            response: 200,
            rooms,
            page,
            next,
            previous,
            maxPage
        })
    })
}

module.exports.deleteRoom = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const room = await KeplerRooms.delete(req.query.id);
        res.json({
            response: 200,
            room
        })
    })
}

module.exports.deleteHand = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        await KeplerItems.deleteHand(req.query.id);
        res.json({
            response: 200
        })
    })
}

module.exports.deleteItem = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const item = await KeplerItems.delete(req.query.id);
        const furni = await KeplerItems.getItemDefinition(item.definition_id);
        res.json({
            response: 200,
            item,
            furni
        })
    })
}