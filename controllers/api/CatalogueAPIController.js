const Security = require("../../services/Security");
const KeplerCatalogueItems = require("../../services/kepler/KeplerCatalogueItems");
const KeplerCataloguePages = require("../../services/kepler/KeplerCataloguePages");
const KeplerItems = require("../../services/kepler/KeplerItems");
const KeplerCataloguePackages = require("../../services/kepler/KeplerCataloguePackages");

module.exports.getCatalogue = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const pages = await KeplerCataloguePages.getAll();
        res.json({
            response: 200,
            pages
        })
    })
}

module.exports.getPage = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const pageId = req.query.id;
        const page = await KeplerCataloguePages.get(pageId);
        res.json({
            response: 200,
            page
        })
    })
}

module.exports.getItem = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const itemId = req.query.id;
        const item = await KeplerCatalogueItems.get(itemId);
        const cataloguePackage = await KeplerCataloguePackages.get(item.sale_code);
        res.json({
            response: 200,
            item,
            cataloguePackage
        })
    })
}

module.exports.getPageItems = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const pageId = req.query.id;

        const items = await KeplerCatalogueItems.getItemsFromPage(pageId);
        let itemDefinitions = [];
        let itemImages = [];
        let itemPackages = [];

        for (let i = 0; i < items.length; i++) {
            if (items[i].is_package) {
                const itemPackage = await KeplerCatalogueItems.getPackage(items[i].sale_code);
                itemPackage && itemPackages.push(itemPackage);
            }
            const itemDefinition = await KeplerItems.getItemDefinition(items[i].definition_id);
            itemDefinition && itemDefinitions.push(itemDefinition);
        }

        for (let i = 0; i < itemDefinitions.length; i++) {
            const itemImage = await KeplerItems.getItemImage(itemDefinitions[i].id);
            itemImage && itemImages.push(itemImage);
        }

        res.json({
            response: 200,
            items,
            itemDefinitions,
            itemImages,
            itemPackages
        })
    })
}

module.exports.postPage = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const pageInfos = req.body.page;
        if (pageInfos.id) {
            await KeplerCataloguePages.edit(pageInfos);
        } else {
            await KeplerCataloguePages.create(pageInfos);
        }
        res.json({
            response: 200
        })
    })
}

module.exports.postPageItem = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const item = req.body.item;
        console.log(item);
        if (item.id) {
            await KeplerCatalogueItems.edit(item);
        } else {
            await KeplerCatalogueItems.create(item);
        }
        res.json({
            response: 200
        })
    })
}

module.exports.getCatalogueHeaders = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const path = require('path');
        const fs = require('fs');
        const directoryPath = path.join(__dirname, '../../ressources/public/images/catalog-headers');
        const files = fs.readdirSync(directoryPath);
        const headers = [];
        files.forEach(function(file) {
            if (file.includes('.gif') || file.includes('.GIF')) {
                const headerName = file.slice(0, -4);
                headers.push(headerName);
            }
        });
        res.json({
            response: 200,
            headers
        })
    })
}

module.exports.getCatalogueTeasers = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const path = require('path');
        const fs = require('fs');
        const directoryPath = path.join(__dirname, '../../ressources/public/images/catalog-teasers');
        const files = fs.readdirSync(directoryPath);
        const teasers = [];
        files.forEach(function(file) {
            if (file.includes('.gif') || file.includes('.GIF')) {
                const teaserName = file.slice(0, -4);
                teasers.push(teaserName);
            }
        });
        res.json({
            response: 200,
            teasers
        })
    })
}



module.exports.getItems = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const items = await KeplerCatalogueItems.getItems();
        res.json({
            response: 200,
            items
        })
    })
}

module.exports.postOrder = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const pages = req.body.pages;
        for (let i = 0; i < pages.length; i++) {
            await KeplerCataloguePages.editOrder(pages[i].id, i);
        }
        res.json({
            response: 200
        })
    })
}

module.exports.postItemsOrder = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const items = req.body.items;
        for (let i = 0; i < items.length; i++) {
            await KeplerCatalogueItems.editOrder(items[i].id, i);
        }
        res.json({
            response: 200
        })
    })
}

module.exports.deletePage = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const pageId = req.query.id;
        await KeplerCataloguePages.delete(pageId);
        res.json({
            response: 200
        })
    })
}

module.exports.deleteItem = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const itemId = req.query.id;
        await KeplerCatalogueItems.delete(itemId);
        res.json({
            response: 200
        })
    })
}