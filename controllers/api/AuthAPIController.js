const KeplerUsers = require("../../services/kepler/KeplerUsers");
const argon2 = require("argon2");

module.exports.getCheckLogged = async (req, res) => {
    if (req.session && req.session.user && req.session.user.rank === 7) {
        const user = await KeplerUsers.getUserByName(req.session.user.username);
        res.json({
            response: 200,
            user
        })
    } else {
        res.json({
            response: 200,
            logged: false
        })
    }
}

module.exports.postLogin = async (req, res) => {
    const user = await KeplerUsers.getUserByName(req.body.username);
    if (user === null) {
        res.json({
            response: 200,
            error: 'userDoesNotExists'
        })
    } else {
        if (await argon2.verify(user.password, req.body.password)) {
            if (user.rank >= 6) {
                req.session.user = user;
                res.json({
                    response: 200,
                    user: req.session.user
                })
            } else {
                res.json({
                    response: 200,
                    error: 'rankNotAllowed'
                })
            }
        } else {
            res.json({
                response: 200,
                error: 'passwordIsNotCorrect'
            })
        }
    }
}

module.exports.getLogout = async (req, res) => {
    req.session.destroy();
    res.json({
        response: 200
    });
}