const Security = require("../../services/Security");
const KeplerRooms = require("../../services/kepler/KeplerRooms");
const KeplerRoomCategories = require("../../services/kepler/KeplerRoomCategories");
const KeplerRoomRights = require("../../services/kepler/KeplerRoomRights");
const KeplerUsers = require("../../services/kepler/KeplerUsers");
const KeplerItems = require("../../services/kepler/KeplerItems");
const KeplerRoomChatlogs = require("../../services/kepler/KeplerRoomChatlogs");

module.exports.postRoom = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const room = req.body.room;
        const categories = await KeplerRoomCategories.get(req.session.user.rank);
        let allowed = false;
        categories.forEach((category) => {
            if (parseInt(category.id) === parseInt(room.category)) {
                allowed = true;
            }
        })
        if (allowed) {
            await KeplerRooms.edit(room);
            res.json({
                response: 200
            })
        } else {
            res.json({
                response: 400,
                error: 'categoryNotAllowed'
            })
        }
    })
}

module.exports.postRoomRight = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const username = req.body.username;
        const roomId = req.body.roomId;
        const user = await KeplerUsers.getUserByName(username.toLowerCase());
        if (user) {
            const alreadyExists = await KeplerRoomRights.checkRight(user.id, roomId);
            if (!alreadyExists) {
                await KeplerRoomRights.add(user.id, roomId);
                res.json({
                    response: 200
                })
            } else {
                res.json({
                    response: 400,
                    error: 'userAlreadyHaveRights'
                })
            }
        } else {
            res.json({
                response: 400,
                error: 'userDoesntExists'
            })
        }
    })
}

module.exports.deleteRoomRight = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const userId = req.query.userId;
        const roomId = req.query.roomId;
        const alreadyExists = await KeplerRoomRights.checkRight(userId, roomId);
        console.log(userId, roomId);
        if (alreadyExists) {
            await KeplerRoomRights.remove(userId, roomId);
        }
        res.json({
            response: 200
        });
    })
}

module.exports.deleteRoomItems = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const roomId = req.query.id;
        await KeplerItems.deleteRoomItems(roomId);
        res.json({
            response: 200
        });
    })
}

module.exports.deleteRoom = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const roomId = req.query.id;
        await KeplerItems.deleteRoomItems(roomId);
        await KeplerRoomChatlogs.deleteRoomChatlogs(roomId);
        await KeplerRoomRights.deleteRoomRights(roomId);
        await KeplerRooms.delete(roomId);
        res.json({
            response: 200
        });
    })
}

module.exports.getRoomRights = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const roomId = req.query.id;
        const users = await KeplerRoomRights.get(roomId);
        res.json({
            response: 200,
            users
        })
    })
}

module.exports.getRoomItems = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const roomId = req.query.id;

        const limit = 20;

        let page = parseInt(req.query.page) || 1;
        const itemsCount = await KeplerItems.countRoomItems(roomId);
        const maxPage = Math.ceil(itemsCount / limit);
        let offset = (page - 1) * limit;
        if (offset >= itemsCount) {
            page = Math.ceil(itemsCount / limit);
            offset = (page - 1) * limit;
        }
        const next = (offset + limit) < itemsCount;
        const previous = offset > 0;

        const { items, itemDefinitions, itemImages } = await KeplerItems.getRoomItems(offset, limit, roomId);

        res.json({
            response: 200,
            page,
            items,
            itemDefinitions,
            itemImages,
            next,
            previous,
            maxPage
        })
    })
}

module.exports.getRoomLogs = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const roomId = req.query.id;

        const limit = 20;

        let page = parseInt(req.query.page) || 1;
        const itemsCount = await KeplerRoomChatlogs.countRoomLogs(roomId);
        const maxPage = Math.ceil(itemsCount / limit);
        let offset = (page - 1) * limit;
        if (offset >= itemsCount) {
            page = Math.ceil(itemsCount / limit);
            offset = (page - 1) * limit;
        }
        const next = (offset + limit) < itemsCount;
        const previous = offset > 0;


        let logs = [],
            users = [];
        if (itemsCount > 0) {
            logs = await KeplerRoomChatlogs.getRoomLogsList(offset, limit, roomId);;
            const usersMap = new Map();
            for (let i = 0; i < logs.length; i++) {
                if (!usersMap.has(logs[i].user_id)) {
                    const user = await KeplerUsers.get(logs[i].user_id)
                    usersMap.set(logs[i].user_id, user);
                }
            }
            users = [];
            usersMap.forEach((user) => {
                users.push(user);
            })
        }

        res.json({
            response: 200,
            page,
            logs,
            users,
            next,
            previous,
            maxPage
        })
    })
}

module.exports.getRoom = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const roomId = req.query.id;
        const room = await KeplerRooms.get(roomId);
        res.json({
            response: 200,
            room
        })
    })
}

module.exports.getCategories = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const categories = await KeplerRoomCategories.get(7);
        res.json({
            response: 200,
            categories
        })
    })
}