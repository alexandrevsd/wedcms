const KeplerItems = require("../../services/kepler/KeplerItems");
const Security = require("../../services/Security");

module.exports.delete = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        await KeplerItems.delete(req.query.id);
        res.json({
            response: 200
        })
    })
}

module.exports.test = async(req, res) => {

    const offset = req.query.offset ? parseInt(req.query.offset) : 0;
    const item = await KeplerItems.getOffset(offset);
    await KeplerItems.giveItem(8, item.definition_id);

    const model = {};
    model.layout = '';
    model.offset = offset + 1;
    model.item = item;


    res.render('views/test', model);
}

module.exports.postTest = async(req, res) => {
    const little_image = req.body.image_name;
    const wedImageId = req.body.item_definition_id;
    KeplerItems.editImage(wedImageId, little_image);
    res.redirect('/api/items/test?offset=' + req.query.offset);
}