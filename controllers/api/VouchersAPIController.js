const Security = require("../../services/Security");
const KeplerVouchers = require("../../services/kepler/KeplerVouchers");

module.exports.getVouchers = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const limit = 20;
        const reqPage = req.query.page || 0;
        const { vouchers, page, next, previous } = await KeplerVouchers.getList(limit, reqPage);
        res.json({
            response: 200,
            vouchers,
            page,
            next,
            previous
        })
    })
}

module.exports.deleteVoucher = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const voucher = await KeplerVouchers.delete(req.query.voucher_code);
        res.json({
            response: 200,
            voucher
        })
    })
}

module.exports.postVoucher = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const voucher = await KeplerVouchers.create(req.body.voucher);
        res.json({
            response: 200,
            voucher
        })
    })
}