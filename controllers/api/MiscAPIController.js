const Security = require("../../services/Security");
const KeplerSettings = require("../../services/kepler/KeplerSettings");
const KeplerUsers = require("../../services/kepler/KeplerUsers");
const KeplerRooms = require("../../services/kepler/KeplerRooms");
const KeplerItems = require("../../services/kepler/KeplerItems");
const WedRanks = require("../../services/wed/WedRanks");
const KeplerUserIpLogs = require("../../services/kepler/KeplerUserIpLogs");

module.exports.getLastIp = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.query.id;
        const ipLog = await KeplerUserIpLogs.getLast(id);
        let ip = (ipLog !== null) ? ipLog.ip_address : '';
        res.json({
            response: 200,
            ip
        })
    })
}

module.exports.getItemDefinitions = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const itemDefinitions = await KeplerItems.getItemDefinitions();
        res.json({
            response: 200,
            itemDefinitions
        })
    })
}

module.exports.getTexts = async(req, res) => {
    const fs = require('fs');
    const file = fs.readFileSync('./ressources/public/v14/external_texts/fr.txt');
    res.set({ 'Content-type': 'text/plain; charset=windows-1252' });
    res.send(file);
}

module.exports.getCounters = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const onlineUsers = parseInt(await KeplerSettings.getSetting('players.online'));
        const registeredUsers = await KeplerUsers.count();
        const rooms = await KeplerRooms.count();
        const furnis = await KeplerItems.count();
        res.json({
            response: 200,
            onlineUsers,
            registeredUsers,
            rooms,
            furnis
        })
    })
}

module.exports.getRankNames = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const rankNames = await WedRanks.getRankNames();
        res.json({
            response: 200,
            rankNames
        })
    })
}

module.exports.postHcMonths = async(req, res) => {
    Security.checkLoggedFromAPI(req, res, async(req, res) => {
        const id = req.body.id;
        const months = parseInt(req.body.months);
        const duration = 60 * 60 * 24 * 31 * months;
        const user = await KeplerUsers.get(id);
        const hcDaysLeft = await KeplerUsers.getHCDaysLeft(user.club_expiration);
        let club_expiration;
        if (hcDaysLeft > 0) {
            club_expiration = await KeplerUsers.addClubTime(id, duration);
        } else {
            club_expiration = await KeplerUsers.setClubTime(id, duration);
        }
        res.json({
            response: 200,
            club_expiration
        })
    })
}