const Security = require("../../services/Security");
const Rcon = require("../../services/Rcon");

module.exports.postHotelAlert = (req, res) => {
    Security.checkLoggedFromAPI(req, res, async (req, res) => {
        if(req.body.message) {
            Rcon.hotelAlert(req.body.message, req.session.user.username);
            res.json({
                response: 200
            })
        } else {
            res.json({
                response: 200,
                error: 'noMessage'
            })
        }
    })
}