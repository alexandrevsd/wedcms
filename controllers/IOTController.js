const Model = require('../services/Model');
const validator = require('email-validator');
const WedHelpMessages = require('../services/wed/WedHelpMessages');

iotModel = async (req, app) => {
    const globalModel = await Model.create(req, app, 'iot', 'iot');
    return globalModel;
}

module.exports.getIOT = async (req, res, app) => {
    const model = await iotModel(req, app);
    // If user already posted (verified by IP)
    if(await WedHelpMessages.getIp(req.headers['x-forwarded-for'] || req.connection.remoteAddress)) {
        // Disable the form
        model.alreadyPosted = true;
    } else {
        // Username is blank
        if(req.session.errorUsername) {
            model.errorUsername = req.session.errorUsername;
            delete req.session.errorUsername;
        }
    }
    res.render('views/iot', model);
}

module.exports.postIOTStep2 = async (req, res, app) => {
    const model = await iotModel(req, app);
    // If username is blank
    if(!req.body.username) {
        req.session.errorUsername = 'Entres ton nom Habbo';
        res.redirect('/iot');
    } else {
        // If user is connected, username is already known
        model.username = (model.user && model.user.username) ? model.user.username : req.body.username;
        res.render('views/iot/step2', model);
    }
}

module.exports.postIOTStep3 = async (req, res, app) => {
    const model = await iotModel(req, app);
    // If user is connected, username is already known
    model.username = (model.user && model.user.username) ? model.user.username : req.body.username;
    model.mail = req.body.mail;
    // If email is correct
    if (validator.validate(req.body.mail)) {
        res.render('views/iot/step3', model);
    } else {
        res.redirect('/iot');
    }
}

module.exports.postIOTStep4 = async (req, res, app) => {
    const model = await iotModel(req, app);
    // If all informations are here
    if(req.body && req.body.username && req.body.message && req.body.mail) {
        // Create a help message in db
        await WedHelpMessages.create({
            username: req.body.username,
            message: req.body.message,
            email: req.body.mail,
            date: Date.now(),
            ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress
        });
        res.render('views/iot/step4', model);
    } else {
        res.redirect('/iot');
    }
}