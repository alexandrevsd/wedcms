const HabboImager = require("../services/HabboImager");

// /habbo-imager/?figure=1000118001270012900121001&reverse=true
// reverse is for changing orientation of habbo

module.exports.getFigure = async (req, res) => {
    const figure = (req.query && req.query.figure) ? req.query.figure : '1000118001270012900121001';
    let img;
    // If we got reverse from URL we change orientation of Habbo
    if (req.query.reverse) {
        img = await HabboImager.getReverse(figure);
    } else {
        if (req.query.little) {
            img = await HabboImager.get(figure);
        } else {
            img = await HabboImager.getBig(figure);
        }
    }
    res.writeHead(200, {
        'Content-Type': 'image/png',
        'Content-Length': img.length
    });
    res.end(img);
}