const Model = require('../../services/Model');

const clubModel = async (req, app) => {
    const model = await Model.create(req, app, 'club');
    model.page = 'club';
    return model;
}

module.exports.getClub = async (req, res, app) => {
    const model = await clubModel(req, app);
    model.submenu = 'club';
    res.render('views/main/club', model);
}

module.exports.getHabboClubBenefits = async (req, res, app) => {
    const model = await clubModel(req, app);
    model.submenu = 'habboClubBenefits';
    res.render('views/main/club/habboClubBenefits', model);
}

module.exports.getJoinHabboClub = async (req, res, app) => {
    const model = await clubModel(req, app);
    model.submenu = 'joinHabboClub';
    res.render('views/main/club/joinHabboClub', model);
}

module.exports.getHabboClubFurni = async (req, res, app) => {
    const model = await clubModel(req, app);
    model.submenu = 'habboClubFurni';
    res.render('views/main/club/habboClubFurni', model);
}