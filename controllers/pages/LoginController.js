const argon2 = require('argon2');
const Security = require("../../services/Security");
const Model = require("../../services/Model");
const KeplerUsers = require("../../services/kepler/KeplerUsers");

module.exports.getLogin = async (req, res, app) => {
    Security.checkNotLogged(req, res, app, async () => {

        // If room from url : add room to form action url
        const room = (req.query.room) ? `&room=${req.query.room}` : '';
        // If redirect from url : add redirect to form action url and room if there is
        const redirect = (req.query.redirect) ? `?redirect=${req.query.redirect}${room}` : '';

        const model = await Model.create(req, app, 'login', 'login');

        // Get error id to show
        model.loginErrorId = req.session.loginErrorId;
        delete req.session.loginErrorId;

        // Login form action url (/login and redirect+room if there are)
        model.loginAction = `/login${redirect}`;
        // If from client (for resizing window)
        model.fromClient = req.query && req.query.redirect && req.query.redirect === 'client';

        res.render('views/login', model);
    });
}

module.exports.postLogin = async (req, res) => {

    // Error redirect link generating (to keep redirect and room forwarding)
    // If room from url : add room to error redirect variable
    const errorRoom = (req.query.room) ? '&room=' + req.query.room : '';
    // If redirect from url : add redirect to error redirect link and room if there is
    const errorRedirect = (req.query.redirect) ? `?redirect=${req.query.redirect}${errorRoom}` : ''
    const errorLink = `/login${errorRedirect}`;

    // Success redirect link generating (to redirect and room forward)
    // If room from url : add room to success redirect variable
    const successRoom = (req.query.room) ? '?room=' + req.query.room : '';
    // If redirect from url : add redirect to success redirect link and room if there is
    const successRedirect = (req.query.redirect) ? `${req.query.redirect}${successRoom}` : ''
    const successLink = `/${successRedirect}`;

    // If nothing post, redirected to error link
    if (!req.body) {
        res.redirect(errorLink);
    } else {
        // Gets user by username
        const user = await KeplerUsers.getUserByName(req.body.username);
        // If user not found, redirect to error link
        if (user === null) {
            req.session.loginErrorId = 1;
            res.redirect(errorLink);
        } else {
            // If user password entered is correct
            if (await argon2.verify(user.password, req.body.password)) {
                // If user is banned, redirect to error link
                if (await KeplerUsers.isBanned(user.id)) {
                    req.session.loginErrorId = 3;
                    res.redirect(errorLink);
                } else {
                    // If everything is correct, user is connected
                    req.session.user = {
                        id: user.id,
                        username: user.username
                    };
                    // Redirect to success link
                    res.redirect(successLink);
                }
            } else {
                // If user password is not correct, redirect to error link
                req.session.loginErrorId = 2;
                res.redirect(errorLink);
            }
        }
    }
}

module.exports.getLogout = (req, res) => {
    if (req.session.user) {
        req.session.destroy();
        res.redirect('/');
    } else {
        res.redirect('/');
    }
}