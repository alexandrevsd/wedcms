const WedAnnouncements = require('../../services/wed/WedAnnouncements');
const WedNews = require('../../services/wed/WedNews');
const KeplerRare = require('../../services/kepler/KeplerRare');
const KeplerUsers = require('../../services/kepler/KeplerUsers');
const Model = require('../../services/Model');

module.exports.getMe = async (req, res, app) => {

    const model = await Model.create(req, app, 'index');
    model.page = 'home';

    // Get announcements to show
    model.announcements = await WedAnnouncements.getLastAnnouncements();
    // Get latest news to show
    model.news = await WedNews.getLatestNews(10);
    // Get rare informations to show
    model.rare = await KeplerRare.getCurrentRareInfos();
    // Get Snowstorm and Battleball best players to show
    model.snowstormPlayers = await KeplerUsers.getSnowstormBestPlayerNames(10);
    model.battleballPlayers = await KeplerUsers.getBattleballBestPlayerNames(10);
    // Get some homepages to show
    model.homepages = await KeplerUsers.getLatestConnectedUsers(8);

    res.render('views/main/index', model);
}