const argon2 = require('argon2');
const Model = require('../../services/Model');
const validator = require('email-validator');
const KeplerUsers = require('../../services/kepler/KeplerUsers');
const WedUsers = require("../../services/wed/WedUsers");

module.exports.getRegister = async (req, res, app) => {
    const model = await Model.create(req, app, 'register', 'register');
    // Error from register form
    if (req.session.errorRegister) {
        model.errorRegister = req.session.errorRegister;
        delete req.session.errorRegister;
    }
    // Keep informations in form from post
    if (req.session.post) {
        model.post = req.session.post;
        delete req.session.post;
    }
    res.render('views/register', model);
}

module.exports.postRegisterSubmit = async (req, res) => {
    // If no informations sent, redirect to register
    if (!req.body) res.redirect('/register');
    else {
        // Sex is M by default
        const sex = (req.body.sex === 'F') ? 'F' : 'M';
        // If 2 password are the same
        if (req.body.password === req.body.passwordRepeat) {
            // If password are more than 8 chars
            if (req.body.password.length >= 8) {
                // If email is correct
                if (validator.validate(req.body.email)) {
                    // If username doesn't exists
                    if (!await KeplerUsers.exists(req.body.username)) {
                        // Create kepler user (for Habbo)
                        const user = await KeplerUsers.create({
                            username: req.body.username,
                            password: await argon2.hash(req.body.password),
                            sex,
                            pool_figure: "",
                        })
                        // Create wedcms user for email and hotel view changing
                        await WedUsers.create({
                            user_id: user.id,
                            email: req.body.email
                        });
                        req.session.user = {
                            id: user.id,
                            username: user.username
                        };
                        res.redirect('/');
                    } else {
                        // If username already exists
                        req.session.errorRegister = 3;
                        req.session.post = req.body;
                        res.redirect('/register');
                    }
                } else {
                    // If email isn't correct
                    req.session.errorRegister = 2;
                    req.session.post = req.body;
                    res.redirect('/register');
                }
            } else {
                // If password is too small
                req.session.errorRegister = 4;
                req.session.post = req.body;
                res.redirect('/register');
            }
        } else {
            // If password aren't the same
            req.session.errorRegister = 1;
            req.session.post = req.body;
            res.redirect('/register');
        }
    }
}
