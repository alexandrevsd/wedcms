const Model = require('../../services/Model');
const WedNews = require('../../services/wed/WedNews');

const newsModel = async (req, app) => {
    const model = await Model.create(req, app, 'news');
    model.page = 'news';
    return model;
}

module.exports.getNews = async (req, res, app) => {
    const model = await newsModel(req, app);
    // Get latest news to show
    model.news = await WedNews.getLatestNews(40);
    res.render('views/main/news', model);
}

module.exports.postSearchNews = async (req, res, app) => {
    const model = await newsModel(req, app);
    // Get searched news
    model.news = await WedNews.getSearchNews(req.body.q, 40);
    res.render('views/main/news', model);
}

module.exports.getOneNews = async (req, res, app) => {
    const model = await newsModel(req, app);
    // Get news from path id
    model.news = await WedNews.getNews(req.params.id);
    // Get latest news to show by side
    model.latestNews = await WedNews.getLatestNews(5);
    res.render('views/main/news/news', model);
}