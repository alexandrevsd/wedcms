const Model = require('../../services/Model');
const KeplerUsers = require('../../services/kepler/KeplerUsers');

module.exports.getRedirect = async (req, res) => {
    // Redirect from a user id homepage to a username homepage
    // The client can only redirect to a player id homepage
    // So it goes gere and then redirect to a normal homepage
    res.redirect('/home/' + req.params.id);
}

async function homepageModel(req, app) {
    const model = await Model.create(req, app, 'homepages');
    model.page = 'homepage';
    return model;
}

module.exports.getHome = async (req, res, app) => {
    const model = await homepageModel(req, app);
    // List of homepages to show
    model.homepages = await KeplerUsers.getLatestConnectedUsers(5);
    res.render('views/main/home', model);
}

module.exports.getHomePage = async (req, res, app) => {
    const model = await homepageModel(req, app);
    // User informations to show in homepage
    model.homeuser = await KeplerUsers.getHomeUser(req.params.id);
    res.render('views/main/home/home', model);
}