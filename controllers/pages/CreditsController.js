const Model = require('../../services/Model');
const WedPaypalItems = require('../../services/wed/WedPaypalItems');
const WedPaypalPayments = require('../../services/wed/WedPaypalPayments');
const KeplerUsers = require('../../services/kepler/KeplerUsers');
const Security = require('../../services/Security');
const Paypal = require('../../services/Paypal');
const KeplerVouchers = require("../../services/kepler/KeplerVouchers");

async function creditsModel(req, app) {
    const model = await Model.create(req, app, 'credits');
    model.page = 'credits';
    return model;
}

module.exports.getCredits = async (req, res, app) => {
    const model = await creditsModel(req, app);
    model.submenu = 'credits';

    // Error voucher (not good or too old)
    if (req.session && req.session.voucherError) {
        model.voucherError = req.session.voucherError;
        delete req.session.voucherError;
    }

    res.render('views/main/credits', model);
}

module.exports.postVoucher = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        if (!req.body || !req.body.code) {
            // If no voucher code sent
            req.session.voucherError = 1;
            res.redirect('/credits');
        } else {
            // Get voucher infos
            const voucher = await KeplerVouchers.get(req.body.code);
            // If voucher exists
            if (voucher) {
                // If voucher is still active
                if (KeplerVouchers.isActive(voucher)) {
                    // Get user infos
                    const infos = await KeplerUsers.getInfos(req.session.user.id);
                    // Set new user credits amount
                    await KeplerUsers.setCredits(req.session.user.id, infos.credits + voucher.credits);
                    // Get items linked to voucher
                    const voucherItems = await KeplerVouchers.getVoucherItems(voucher);
                    // If there are items
                    if (voucherItems) {
                        // Give items to the user
                        await KeplerUsers.giveVoucherItems(req.session.user.id, voucherItems);
                    }
                    // Delete voucher if it's single use one
                    await KeplerVouchers.deleteSingleUse(voucher);
                    req.session.voucherSuccess = true;
                    res.redirect('/credits');
                } else {
                    // If voucher is no longer active
                    req.session.voucherError = 2;
                    res.redirect('/credits');
                }
            } else {
                // If voucher doesn't exists
                req.session.voucherError = 1;
                res.redirect('/credits');
            }
        }
    })
}

module.exports.getUseMyCredits = async (req, res, app) => {
    const model = await creditsModel(req, app);
    model.submenu = 'useMyCredits';
    res.render('views/main/credits/useMyCredits', model);
}

module.exports.getPaypal = async (req, res, app) => {
    const model = await creditsModel(req, app);
    model.paypalItems = await WedPaypalItems.getAll();

    // Success paypal payment
    if(req.session.successPaypal) {
        model.successPaypal = true;
        delete req.session.successPaypal;
    }

    res.render('views/main/credits/paypal', model);
}

module.exports.getSuccess = async (req, res) => {
Security.checkLogged(req, res, async (req, res) => {
    // Check payment informations from request
    await Paypal.paymentVerification(req, async (err, pay, paypalItem, paymentInfo) => {
        // If payment accepted
        if(pay) {
            // Get user infos to get credits amount
            const userInfos = await KeplerUsers.getInfos(req.session.user.id);
            // Set new credits amount to user
            await KeplerUsers.setCredits(req.session.user.id, userInfos.credits + paypalItem.credits);
            // Delete paypal payment link
            await WedPaypalPayments.delete(paymentInfo.paymentId);

            req.session.successPaypal = true;
            res.redirect('/credits/paypal');
        } else {
            // If payement denied
            if(err) console.error(err);
            res.redirect('/credits/paypal?error=true');
        }
    });
})

}