const Model = require('../../services/Model');

const hotelModel = async (req, app) => {
    const model = await Model.create(req, app, 'hotel');
    model.page = 'hotel';
    return model;
}

module.exports.getHotel = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'hotel';
    res.render('views/main/hotel', model);
}

module.exports.getWelcomeToHabboHotel = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'welcomeToHabboHotel';
    res.render('views/main/hotel/welcomeToHabboHotel', model);
}

module.exports.getHowToGetStarted = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'welcomeToHabboHotel';
    model.subpage = 'howToGetStarted';
    res.render('views/main/hotel/welcomeToHabboHotel/howToGetStarted', model);
}

module.exports.getNavigator = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'welcomeToHabboHotel';
    model.subpage = 'navigator';
    res.render('views/main/hotel/welcomeToHabboHotel/navigator', model);
}

module.exports.getMeeting = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'welcomeToHabboHotel';
    model.subpage = 'meeting';
    res.render('views/main/hotel/welcomeToHabboHotel/meeting', model);
}

module.exports.getRoom = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'welcomeToHabboHotel';
    model.subpage = 'room';
    res.render('views/main/hotel/welcomeToHabboHotel/room', model);
}

module.exports.getHelpSafety = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'welcomeToHabboHotel';
    model.subpage = 'helpSafety';
    res.render('views/main/hotel/welcomeToHabboHotel/helpSafety', model);
}

module.exports.getChatting = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'welcomeToHabboHotel';
    model.subpage = 'chatting';
    res.render('views/main/hotel/welcomeToHabboHotel/chatting', model);
}

module.exports.getFurniture = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'furni';
    res.render('views/main/hotel/furniture', model);
}

module.exports.getCatalogue = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'catalogue';
    res.render('views/main/hotel/furniture/catalogue', model);
}

module.exports.getCatalogue1 = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'catalogue1';
    res.render('views/main/hotel/furniture/catalogue1', model);
}

module.exports.getCatalogue2 = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'catalogue2';
    res.render('views/main/hotel/furniture/catalogue2', model);
}

module.exports.getCatalogue4 = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'catalogue4';
    res.render('views/main/hotel/furniture/catalogue4', model);
}

module.exports.getDecorationExamples = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'decorationExamples';
    res.render('views/main/hotel/furniture/decorationExamples', model);
}

module.exports.getStaffFavourite = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'staffFavourite';
    res.render('views/main/hotel/furniture/staffFavourite', model);
}

module.exports.getTrading = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'furniture';
    model.subpage = 'trading';
    res.render('views/main/hotel/furniture/trading', model);
}

module.exports.getPets = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'pets';
    model.subpage = 'pets';
    res.render('views/main/hotel/pets', model);
}

module.exports.getTakingCareOfYourPet = async (req, res, app) => {
    const model = await hotelModel(req, app);
    model.submenu = 'pets';
    model.subpage = 'takingCareOfYourPet';
    res.render('views/main/hotel/pets/takingCareOfYourPet', model);
}