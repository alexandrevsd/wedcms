const Model = require('../../services/Model');
const Security = require('../../services/Security');
const HabboImager = require('../../services/HabboImager');
const KeplerUsers = require("../../services/kepler/KeplerUsers");
const WedUsers = require("../../services/wed/WedUsers");
const argon2 = require("argon2");

module.exports.getPreferences = async (req, res, app) => {
    Security.checkLogged(req, res, async (req, res, app) => {
        const model = await Model.create(req, app, 'preferences');
        model.subpage = 'preferences';

        // Success motto changing
        if(req.session.successMotto) {
            model.successMotto = true;
            delete req.session.successMotto;
        }
        // Success password changing
        if(req.session.successPassword) {
            model.successPassword = true;
            delete req.session.successPassword;
        }
        // Error password changing
        if(req.session.errorPassword) {
            model.errorPassword = req.session.errorPassword;
            delete req.session.errorPassword;
        }

        res.render('views/main/preferences', model);
    }, app)
}

module.exports.postChangeMotto = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        // If motto sent
        if (req.body && req.body.motto) {
            req.session.successMotto = true;
            // Set new motto to user
            await KeplerUsers.setMotto(req.session.user.id, req.body.motto);
        }
        res.redirect('/preferences');
    })
}

module.exports.postChangePassword = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        // If actual, new and new password repeated are sent
        if (req.body && req.body.actualPassword && req.body.newPassword && req.body.newPasswordRepeat) {
            // If new password is higher than 8 chars
            if (req.body.newPassword.length >= 8) {
                // If new password are the same
                if (req.body.newPassword === req.body.newPasswordRepeat) {
                    // If acutal password is correct
                    if (await argon2.verify(await KeplerUsers.getPassword(req.session.user.id), req.body.actualPassword)) {
                        // Set new password to user
                        await KeplerUsers.setPassword(req.session.user.id, await argon2.hash(req.body.newPassword));
                        req.session.successPassword = true;
                        res.redirect('/preferences');
                    } else {
                        // If password isn't correct
                        req.session.errorPassword = 3;
                        res.redirect('/preferences');
                    }
                } else {
                    // If new password aren't the same
                    req.session.errorPassword = 2;
                    res.redirect('/preferences');
                }
            } else {
                // If new password is too small
                req.session.errorPassword = 1;
                res.redirect('/preferences');
            }
        } else {
            // If informations wasn't sent correctly
            res.redirect('/preferences');
        }
    })
}

module.exports.getPreferencesLook = async (req, res, app) => {
    Security.checkLogged(req, res, async (req, res, app) => {
        const infos = await KeplerUsers.getInfos(req.session.user.id);
        const model = await Model.create(req, app, 'preferences');
        model.subpage = 'look';

        // Get all sprites and colors of each sprites
        model.sprites = HabboImager.getAllSprites(infos.sex);
        model.colors = HabboImager.getAllSpritesColors(model.sprites, infos.sex);

        // Decrypt user figure to put in habbo imager
        const decrypt = HabboImager.decrypt(model.user.figure);
        // Create habbo imager default image
        model.habboInfos = {
            haircut: {sprite: decrypt.get('haircut').sprite, color: decrypt.get('haircut').color},
            face: {sprite: decrypt.get('face').sprite, color: decrypt.get('face').color},
            top: {sprite: decrypt.get('top').sprite, color: decrypt.get('top').color},
            pants: {sprite: decrypt.get('pants').sprite, color: decrypt.get('pants').color},
            shoes: {sprite: decrypt.get('shoes').sprite, color: decrypt.get('shoes').color},
        }

        // Success figure changing
        if(req.session.successFigure) {
            model.successFigure = true;
            delete req.session.successFigure;
        }

        // Success sex changing
        if(req.session.successSex) {
            model.successSex = true;
            delete req.session.successSex;
        }
        res.render('views/main/preferences/look', model);
    }, app)
}

module.exports.postChangeSex = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        // If view sent
        if (req.body && req.body.sex) {
            req.session.successSex = true;
            const sex = req.body.sex === 'F' ? 'F' : 'M';
            // Set new view to user
            await KeplerUsers.setSex(req.session.user.id, sex);
        }
        res.redirect('/preferences/look');
    })
}

module.exports.postChangeFigure = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        // If figure sent
        if (req.body && req.body.figure) {
            req.session.successFigure = true;
            // Set new figure to user
            await KeplerUsers.setFigure(req.session.user.id, req.body.figure);
        }
        res.redirect('/preferences/look');
    })
}

module.exports.getHotelView = async (req, res, app) => {
    Security.checkLogged(req, res, async (req, res, app) => {
        const model = await Model.create(req, app, 'preferences');
        model.subpage = 'view';

        // Success view changing
        if(req.session.successView) {
            model.successView = true;
            delete req.session.successView;
        }
        res.render('views/main/preferences/view', model);
    }, app)
}

module.exports.postChangeView = async (req, res) => {
    // Checks if user is logged
    Security.checkLogged(req, res, async (req, res) => {
        // If view sent
        if (req.body && req.body.view) {
            req.session.successView = true;
            // Set new view to user
            await WedUsers.setView(req.session.user.id, req.body.view);
        }
        res.redirect('/preferences/view');
    })
}

module.exports.getHotelViews = async (req, res, app) => {
    Security.checkLogged(req, res, async (req, res, app) => {
        const model = await Model.create(req, app, 'preferences');
        res.render('views/main/preferences/views', model);
    }, app)
}