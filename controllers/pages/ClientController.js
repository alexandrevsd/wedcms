const Security = require('../../services/Security');
const KeplerUsers = require('../../services/kepler/KeplerUsers');
const Model = require('../../services/Model');

module.exports.getClient = (req, res, app) => {
    // Set session redirect to client if user is disconnected
    req.session.redirect = 'client';
    // Set session for room forward if user is disconnected
    req.session.room = req.query.room;
    Security.checkLogged(req, res, async () => {
        // Create a new token for SSO login
        const token = await Security.getToken();
        // Save the new token in DB to send to server
        await KeplerUsers.setToken(req.session.user.id, token);

        const model = await Model.create(req, app, 'client', 'client');
        model.sso = token;
        model.room = req.query.room;
        model.externalVarsLink = `/user/ExternalVars?view=${model.user.view}`;
        model.keplerServerHostname = process.env.KEPLER_SERVER_HOSTNAME;
        model.keplerServerPort = process.env.KEPLER_SERVER_PORT;
        model.keplerMusPort = process.env.KEPLER_MUS_PORT;

        // Deleting redirection to client and room for client
        delete req.session.redirect;
        delete req.session.room;

        res.render('views/client', model);
    });
}