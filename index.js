// Environment variables configured from .env file
require('dotenv').config();

const LogoGenerator = require('./services/LogoGenerator');
LogoGenerator.create('WedCMS')

const Text = require('./services/Texts');
Text.language = process.env.LANGUAGE || 'en';

// Creating server with init method
const Server = require('./services/Server');
new Server().init();

const ExtGen = require('./services/ExternalVarsGenerator');
const values = new Map();
values.set('view', 'fr');
ExtGen.generateFile(values);
//ExtGen.changeView('fr', 'fr');