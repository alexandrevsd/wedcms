const express = require('express');
const UsersAPI = require("../controllers/api/UsersAPIController");
const CatalogueAPI = require("../controllers/api/CatalogueAPIController");
const ItemsAPI = require("../controllers/api/ItemsAPIController");
const VouchersAPI = require("../controllers/api/VouchersAPIController");
const AuthAPI = require("../controllers/api/AuthAPIController");
const RconAPI = require("../controllers/api/RconAPIController");
const MiscAPI = require("../controllers/api/MiscAPIController");
const RoomsAPI = require("../controllers/api/RoomsAPIController");
const APIRouter = express.Router();

APIRouter.post('/api/auth/login', AuthAPI.postLogin);
APIRouter.get('/api/auth/logout', AuthAPI.getLogout);
APIRouter.get('/api/auth/checkLogged', AuthAPI.getCheckLogged);


// Users API
APIRouter.get('/api/users', UsersAPI.getUsers); // Get users list ?page=
APIRouter.get('/api/user', UsersAPI.getUser); // Get user ?id=
APIRouter.get('/api/user/ipLogs', UsersAPI.getIpLogs); // Get last 20 user's ip logs ?id=
APIRouter.get('/api/user/vouchersList', UsersAPI.getVouchersList); // Get last 20 user's vouchers history ?id=
APIRouter.get('/api/user/bansList', UsersAPI.getBansList); // Get last user's ban ?id=
APIRouter.get('/api/user/hand', UsersAPI.getHand); // Get user's hand ?id=&page=
APIRouter.get('/api/user/rooms', UsersAPI.getRooms); // Get user's rooms ?id=&page=
APIRouter.get('/api/user/badges', UsersAPI.getBadges); // ?id=
APIRouter.get('/api/user/console', UsersAPI.getConsole);
APIRouter.post('/api/user', UsersAPI.postUser); // Post user for editing or new {user}
APIRouter.post('/api/user/ban', UsersAPI.ban);
APIRouter.post('/api/user/badges', UsersAPI.postBadges);
APIRouter.post('/api/user/unban', UsersAPI.postUnban);
APIRouter.delete('/api/user/hc', UsersAPI.deleteUserHc);
APIRouter.delete('/api/user/hand', UsersAPI.deleteHand);
APIRouter.delete('/api/item', UsersAPI.deleteItem);
APIRouter.delete('/api/users/room', UsersAPI.deleteRoom);
APIRouter.delete('/api/user', UsersAPI.deleteUser);
APIRouter.get('/api/user/test', UsersAPI.test);

APIRouter.get('/api/rooms/categories', RoomsAPI.getCategories);
APIRouter.get('/api/room', RoomsAPI.getRoom);
APIRouter.get('/api/room/rights', RoomsAPI.getRoomRights); // ?id=
APIRouter.get('/api/room/items', RoomsAPI.getRoomItems); // ?id=
APIRouter.get('/api/room/logs', RoomsAPI.getRoomLogs); // ?id=
APIRouter.post('/api/room', RoomsAPI.postRoom);
APIRouter.post('/api/room/right', RoomsAPI.postRoomRight);
APIRouter.delete('/api/room/right', RoomsAPI.deleteRoomRight);
APIRouter.delete('/api/room/items', RoomsAPI.deleteRoomItems);
APIRouter.delete('/api/room', RoomsAPI.deleteRoom);

APIRouter.get('/api/vouchers', VouchersAPI.getVouchers);
APIRouter.delete('/api/voucher', VouchersAPI.deleteVoucher);
APIRouter.post('/api/voucher', VouchersAPI.postVoucher);

APIRouter.get('/api/catalogue', CatalogueAPI.getCatalogue);
APIRouter.get('/api/catalogue/headers', CatalogueAPI.getCatalogueHeaders);
APIRouter.get('/api/catalogue/teasers', CatalogueAPI.getCatalogueTeasers);
APIRouter.get('/api/catalogue/items', CatalogueAPI.getItems);
APIRouter.get('/api/catalogue/item', CatalogueAPI.getItem);
APIRouter.get('/api/catalogue/page', CatalogueAPI.getPage);
APIRouter.get('/api/catalogue/page/items', CatalogueAPI.getPageItems);
APIRouter.post('/api/catalogue/page/item', CatalogueAPI.postPageItem);
APIRouter.post('/api/catalogue/order', CatalogueAPI.postOrder);
APIRouter.post('/api/catalogue/items/order', CatalogueAPI.postItemsOrder);
APIRouter.post('/api/catalogue/page', CatalogueAPI.postPage);
APIRouter.delete('/api/catalogue/page', CatalogueAPI.deletePage);
APIRouter.delete('/api/catalogue/item', CatalogueAPI.deleteItem);



APIRouter.get('/api/misc/lastIp', MiscAPI.getLastIp);
APIRouter.get('/api/misc/counters', MiscAPI.getCounters);
APIRouter.get('/api/misc/rankNames', MiscAPI.getRankNames);
APIRouter.post('/api/misc/hcMonths', MiscAPI.postHcMonths);
APIRouter.get('/api/misc/itemDefinitions', MiscAPI.getItemDefinitions);
APIRouter.get('/api/misc/texts', MiscAPI.getTexts);

APIRouter.delete('/api/item', ItemsAPI.delete);

APIRouter.post('/api/rcon/sendHotelAlert', RconAPI.postHotelAlert);

module.exports = APIRouter;