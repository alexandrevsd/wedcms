const express = require('express');
const router = express.Router();
const App = require('../services/App');
const app = new App();

const Imager = require('../controllers/ImagerController');
const Preferences = require('../controllers/pages/PreferencesController');
const News = require('../controllers/pages/NewsController');
const Hotel = require('../controllers/pages/HotelController');
const Homepages = require('../controllers/pages/HomepagesController');
const Credits = require('../controllers/pages/CreditsController');
const Club = require('../controllers/pages/ClubController');
const IOT = require('../controllers/IOTController');
const User = require("../controllers/UserAPIController");
const Client = require("../controllers/pages/ClientController");
const Login = require('../controllers/pages/LoginController');
const Register = require('../controllers/pages/RegisterController');
const Index = require('../controllers/pages/IndexController');

function getApp(req, res, callback) {
    callback(req, res, app);
}

///////////////////////////
//      HABBO IMAGER     //
///////////////////////////

router.get('/habbo-imager', (req, res) => getApp(req, res, Imager.getFigure));


///////////////////////////
//          IOT          //
///////////////////////////

router.get('/iot', (req, res) => getApp(req, res, IOT.getIOT));
router.get('/iot/2', (_, res) => res.redirect('/iot'));
router.get('/iot/3', (_, res) => res.redirect('/iot'));
router.get('/iot/4', (_, res) => res.redirect('/iot'));
router.post('/iot/2', (req, res) => getApp(req, res, IOT.postIOTStep2));
router.post('/iot/3', (req, res) => getApp(req, res, IOT.postIOTStep3));
router.post('/iot/4', (req, res) => getApp(req, res, IOT.postIOTStep4));

///////////////////////////
//    AUTHENTIFICATION   //
///////////////////////////

// Login
router.get('/login', (req, res) => getApp(req, res, Login.getLogin));
router.post('/login', (req, res) => getApp(req, res, Login.postLogin));
router.get('/logout', (req, res) => getApp(req, res, Login.getLogout));

// Register
router.post('/register', (req, res) => getApp(req, res, Register.getRegister));
router.get('/register', (req, res) => getApp(req, res, Register.getRegister));
router.post('/registerSubmit', (req, res) => getApp(req, res, Register.postRegisterSubmit));

///////////////////////////
//       USER'S API      //
///////////////////////////

router.get('/user/habbosConnected', (req, res) => getApp(req, res, User.getHabbosConnected));
router.get('/user/subscribeClub', (req, res) => getApp(req, res, User.getSubscribeClub));
router.get('/user/buyRare', (req, res) => getApp(req, res, User.getBuyRare));
router.get('/user/buyCredits/:id', (req, res) => getApp(req, res, User.getBuyCredits));
router.get('/user/externalVars', (req, res) => getApp(req, res, User.getExternalVars));


///////////////////////////
//      USER'S PAGES     //
///////////////////////////

// Preferences
router.get('/preferences', (req, res) => getApp(req, res, Preferences.getPreferences));
router.post('/preferences/changeMotto', (req, res) => getApp(req, res, Preferences.postChangeMotto));
router.post('/preferences/changePassword', (req, res) => getApp(req, res, Preferences.postChangePassword));
router.get('/preferences/look', (req, res) => getApp(req, res, Preferences.getPreferencesLook));
router.post('/preferences/changeSex', (req, res) => getApp(req, res, Preferences.postChangeSex));
router.post('/preferences/changeFigure', (req, res) => getApp(req, res, Preferences.postChangeFigure));
router.get('/preferences/view', (req, res) => getApp(req, res, Preferences.getHotelView));
router.post('/preferences/changeView', (req, res) => getApp(req, res, Preferences.postChangeView));
router.get('/preferences/views', (req, res) => getApp(req, res, Preferences.getHotelViews));

// Credits
router.post('/credits/voucher', (req, res) => getApp(req, res, Credits.postVoucher));

///////////////////////////
//      MAIN WEBSITE     //
///////////////////////////

// Client
router.get('/client', (req, res) => getApp(req, res, Client.getClient));

// Index
router.get('/', (req, res) => getApp(req, res, Index.getMe));

// Hotel Tab
router.get('/hotel', (req, res) => getApp(req, res, Hotel.getHotel));
router.get('/hotel/welcome_to_habbo_hotel', (req, res) => getApp(req, res, Hotel.getWelcomeToHabboHotel));
router.get('/hotel/welcome_to_habbo_hotel/how_to_get_started', (req, res) => getApp(req, res, Hotel.getHowToGetStarted));
router.get('/hotel/welcome_to_habbo_hotel/navigator', (req, res) => getApp(req, res, Hotel.getNavigator));
router.get('/hotel/welcome_to_habbo_hotel/meeting', (req, res) => getApp(req, res, Hotel.getMeeting));
router.get('/hotel/welcome_to_habbo_hotel/room', (req, res) => getApp(req, res, Hotel.getRoom));
router.get('/hotel/welcome_to_habbo_hotel/help_safety', (req, res) => getApp(req, res, Hotel.getHelpSafety));
router.get('/hotel/welcome_to_habbo_hotel/chatting', (req, res) => getApp(req, res, Hotel.getChatting));
router.get('/hotel/furniture', (req, res) => getApp(req, res, Hotel.getFurniture));
router.get('/hotel/furniture/catalogue', (req, res) => getApp(req, res, Hotel.getCatalogue));
router.get('/hotel/furniture/catalogue1', (req, res) => getApp(req, res, Hotel.getCatalogue1));
router.get('/hotel/furniture/catalogue2', (req, res) => getApp(req, res, Hotel.getCatalogue2));
router.get('/hotel/furniture/catalogue4', (req, res) => getApp(req, res, Hotel.getCatalogue4));
router.get('/hotel/furniture/decoration_examples', (req, res) => getApp(req, res, Hotel.getDecorationExamples));
router.get('/hotel/furniture/staff_favourite', (req, res) => getApp(req, res, Hotel.getStaffFavourite));
router.get('/hotel/furniture/trading', (req, res) => getApp(req, res, Hotel.getTrading));
router.get('/hotel/pets', (req, res) => getApp(req, res, Hotel.getPets));
router.get('/hotel/pets/taking_care_of_your_pet', (req, res) => getApp(req, res, Hotel.getTakingCareOfYourPet));
router.get('/hotel/info_bus', (req, res) => getApp(req, res, Hotel.getInfoBus));
router.get('/hotel/version12', (req, res) => getApp(req, res, Hotel.getVersion12));

// News tab
router.get('/news', (req, res) => getApp(req, res, News.getNews));
router.post('/news', (req, res) => getApp(req, res, News.postSearchNews));
router.get('/news/:id', (req, res) => getApp(req, res, News.getOneNews));

// Homepages tab
router.get('/home', (req, res) => getApp(req, res, Homepages.getHome));
router.get('/home/redirect/:id', (req, res) => getApp(req, res, Homepages.getRedirect));
router.get('/home/:id', (req, res) => getApp(req, res, Homepages.getHomePage));

// Credits tab
router.get('/credits', (req, res) => getApp(req, res, Credits.getCredits));
router.get('/credits/use_my_credits', (req, res) => getApp(req, res, Credits.getUseMyCredits));
router.get('/credits/paypal', (req, res) => getApp(req, res, Credits.getPaypal));
router.get('/credits/success', (req, res) => getApp(req, res, Credits.getSuccess));

// Club tab
router.get('/club', (req, res) => getApp(req, res, Club.getClub));
router.get('/club/habbo_club_benefits', (req, res) => getApp(req, res, Club.getHabboClubBenefits));
router.get('/club/join_habbo_club', (req, res) => getApp(req, res, Club.getJoinHabboClub));
router.get('/club/habbo_club_furni', (req, res) => getApp(req, res, Club.getHabboClubFurni));

module.exports = router;