const path = require('path');
const express = require('express');
const housekeepingRouter = express.Router();

housekeepingRouter.get('/housekeeping', (req,res) => {
    res.sendFile(path.join(__dirname, '../ressources/housekeeping.html'));
});

housekeepingRouter.get('/housekeeping/*', (req,res) => {
    res.sendFile(path.join(__dirname, '../ressources/housekeeping.html'));
});

module.exports = housekeepingRouter;