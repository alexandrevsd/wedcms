const path = require('path');
const fs = require("fs");

module.exports.asyncForEach = async function(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

module.exports.join = (pathToJoin) => {
    return path.join(__dirname, pathToJoin);
}

module.exports.decodeHabboTxtFile = (path) => {
    const lines = fs.readFileSync(path).toString().split('\n');
    const vars = new Map();
    lines.forEach(line => {
        const decrypt = line.split('=');
        vars.set(decrypt[0], decrypt[1]);
    })
    return vars;
}

module.exports.encodeHabboTxtFile = (vars) => {
    const lines = [];
    vars.forEach((value, key) => {
        lines.push(key + '=' + value);
    })
    return lines.join('\r\n');
}

module.exports.formatDate = function(date) {
    const fromDate = new Date(date);

    let day = fromDate.getDay();
    let month = fromDate.getMonth();
    let year = (fromDate.getFullYear() + '').substr(2, 2);

    if (day < 10) {
        day = '0' + day;
    }

    if (month < 10) {
        month = '0' + month;
    }

    return `${day}/${month}/${year}`;
}