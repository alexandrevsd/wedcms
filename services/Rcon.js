const net = require('net');
const pack = require('locutus/php/misc/pack');

class Rcon {

    /**
     * Sends a live hotel alert
     * @param {string} message - Message content
     * @param {string} sender - Author of the message
     */
    static hotelAlert(message, sender = '') {
        const data = new Map([
            ['message', message]
        ]);
        if (sender !== '') data.set('sender', sender);
        this._sendRcon("hotel_alert", data);
    }

    /**
     * Refreshes look of a user
     * @param {int} userId - User id to refresh
     */
    static refreshLooks(userId) {
        this._refresh('looks', userId);
    }

    /**
     * Refreshes hand of a user
     * @param {int} userId - Hand's user id to refresh
     */
    static refreshHand(userId) {
        this._refresh('hand', userId);
    }

    /**
     * Refreshes credits of a user
     * @param {int} userId - Credit's user id to refresh
     */
    static refreshCredits(userId) {
        this._refresh('credits', userId);
    }

    /**
     * Refreshes club subscription of a user
     * @param {int} userId - HC Sub's user id to refresh
     */
    static refreshClub(userId) {
        this._refresh('club', userId);
    }

    /**
     * Builds a RCON refresh order
     * @param refreshType - Type of the refresh order
     * @param userId - Refresh effect's user id
     * @private
     */
    static _refresh(refreshType, userId) {
        this._sendRcon(`refresh_${refreshType}`, new Map([
            ['userId', userId.toString()]
        ]));
    }

    /**
     * Sends a RCON order
     * @param {string} header - Header of the order
     * @param {Map} parameters - Parameters of the order
     * @private
     */
    static _sendRcon(header, parameters) {
        // Quackter PHP's code converted to NodeJS
        const command = this._build(header, parameters);
        const socket = new net.Socket();
        socket.connect(process.env.RCON_PORT, process.env.RCON_HOST, function() {
            socket.write(command, process.env.RCON_ENCODING);
        });
    }

    /**
     * Builds RCON order
     * @param {string} header - Header to build
     * @param {Map} parameters - Parameters to build
     * @returns {string} - Buffer translated to server's language communication
     * @private
     */
    static _build(header, parameters) {
        // Quackster PHP's code converted to NodeJS
        let message = "";
        message += pack('N', header.length);
        message += header;
        message += pack('N', parameters.size);
        parameters.forEach((value, key) => {
            message += pack('N', key.length);
            message += key;
            message += pack('N', value.length);
            message += value;
        });
        let buffer = '';
        buffer += pack('N', message.length);
        buffer += message;
        return buffer;
    }

}

module.exports = Rcon;