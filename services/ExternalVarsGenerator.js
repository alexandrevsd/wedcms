const RESSOURCES_PATH = './ressources';
const {decodeHabboTxtFile, join, encodeHabboTxtFile} = require('../utils');

class ExternalVarsGenerator {

    /**
     * Generates an external vars txt file for Habbo
     * @param valuesToChange - Map with values to change (actually only view)
     * @return {Buffer} - Return the external vars txt file with edited values
     */
    static generateFile(valuesToChange = new Map()) {
        // Decode txt file to var => value in map
        const vars = decodeHabboTxtFile(join(RESSOURCES_PATH + '/texts/external_vars.txt'));

        // For each value to change
        valuesToChange.forEach((value, key) => {
            // If value to change is view
            if(key === 'view') {
                // Set the var "cast.entry.16" to hh_entry_${value}
                vars.set('cast.entry.16', 'hh_entry_' + value);
            }
        })

        // Return the new txt file
        return encodeHabboTxtFile(vars);
    }

}

module.exports = ExternalVarsGenerator;