const models = require('../../models');
const { UserIpLog } = models;

class KeplerUserIpLogs {

    static async getLast(user_id) {
        return await UserIpLog.findOne({
            where: { user_id },
            order: [
                ['created_at', 'DESC']
            ]
        });
    }

    static async getList(user_id) {
        return await UserIpLog.findAll({
            where: { user_id },
            order: [
                ['created_at', 'DESC']
            ],
            limit: 20
        });
    }

}

module.exports = KeplerUserIpLogs;