const models = require('../../models');
const {MessengerFriend, User} = models;
const KeplerUsers = require("./KeplerUsers");
const {asyncForEach} = require("../../utils");

class KeplerMessengerFriends {

    /**
     * Gets a user from friendId
     * @param friendId
     * @returns {Promise<*>}
     */
    static async getFriend(friendId) {
        return await KeplerUsers.getUser(friendId);
    }

    /**
     * Gets friends list from a user_id
     * @param {int} from_id - User id to lists friends
     * @returns {Promise<{figure: *, username: *}[]>}
     */
    static async getFrom(from_id) {
        const friendsIds = await MessengerFriend.findAll({
            limit: 10,
            where: {from_id}
        });
        const friends = [];
        await asyncForEach(friendsIds, async friendId => {
            friends.push(await User.findOne({
                where:{id: friendId.to_id}
            }));
        })
        return friends.map(friend => {
            return {
                username: friend.username,
                figure: friend.figure
            }
        })
    }

}

module.exports = KeplerMessengerFriends;