const models = require('../../models');
const sequelize = require("sequelize");
const Rcon = require("../Rcon");
const KeplerMessengerFriends = require("./KeplerMessengerFriends");
const KeplerRooms = require("./KeplerRooms");
const { Op } = require("sequelize");
const { asyncForEach } = require("../../utils");
const KeplerUserIpLogs = require('./KeplerUserIpLogs');
const { User, UserBadge, UserBan, CatalogueItem, Item, WedUser, VoucherHistory } = models;
const argon2 = require('argon2');
const KeplerUserBadges = require('./KeplerUserBadges');

class KeplerUsers {

    static async setBadges(user_id, badges) {
        await KeplerUserBadges.deleteAll(user_id);
        for (let i = 0; i < badges.length; i++) {
            await KeplerUserBadges.create(user_id, badges[i]);
        }
    }

    static async edit(userToEdit) {
        const user = await KeplerUsers.get(userToEdit.id);
        if (userToEdit.password !== '') {
            const hashedPassword = await argon2.hash(userToEdit.password);
            user.password = hashedPassword;
        }
        user.rank = parseInt(userToEdit.rank);
        user.motto = userToEdit.motto;
        user.console_motto = userToEdit.console_motto;
        if (user.credits != userToEdit.credits) {
            Rcon.refreshCredits(user.id);
            user.credits = userToEdit.credits;
        }
        user.tickets = userToEdit.tickets;
        user.film = userToEdit.film;
        user.battleball_points = userToEdit.battleball_points;
        user.snowstorm_points = userToEdit.snowstorm_points;
        await user.save();
        return user;
    }

    static async create(values) {
        return await User.create(values);
    }

    static async delete(id) {
        const user = await KeplerUsers.get(id);
        await user.destroy();
    }

    static async ban(id, message, length) {
        const banned_until = (Date.now() / 1000) + (length * 60 * 60);
        const alreadyBan = await UserBan.findOne({ where: { banned_value: id } });
        if (!message) message = '';
        if (alreadyBan) {
            alreadyBan.banned_until = banned_until;
            alreadyBan.message = message;
            alreadyBan.save();
        } else {
            await UserBan.create({
                ban_type: 'USER_ID',
                banned_value: id,
                message,
                banned_until
            });
        }
    }

    static async getVouchersList(id) {
        return await VoucherHistory.findAll({
            limit: 20,
            where: { user_id: id },
            order: [
                ['used_at', 'DESC']
            ]
        });
    }

    static async unban(id) {
        const activeBans = await UserBan.findAll({
            where: {
                ban_type: 'USER_ID',
                banned_value: id,
                banned_until: {
                    [Op.gt]: parseInt(Date.now() / 1000)
                }
            }
        })
        for (let i = 0; i < activeBans.length; i++) {
            activeBans[i].banned_until = parseInt(Date.now() / 1000);
            await activeBans[i].save();
        }
    }

    /**
     * Gets a user
     * @param id
     * @return {Promise<Model<any, any>>}
     */
    static async get(id) {
        return await User.findOne({
            where: { id },
            include: UserBadge
        })
    }

    static async count() {
        return await User.count();
    }

    static async getList(limit, page) {
        // Pagination
        page = parseInt(page);
        const usersCount = await KeplerUsers.count();
        const maxPage = Math.ceil(usersCount / limit);
        let offset = (page - 1) * limit;
        if (offset >= usersCount) {
            page = Math.ceil(usersCount / limit);
            offset = (page - 1) * limit;
        }
        const next = (offset + limit) < usersCount;
        const previous = offset > 0;
        // Get users
        const users = await User.findAll({
            limit,
            offset,
            order: [
                ['id', 'ASC']
            ]
        });
        return { users, page, next, previous, maxPage };
    }

    /**
     * Sets a new sso token to a user
     * @param id
     * @param token
     * @returns {Promise<void>}
     */
    static async setToken(id, token) {
        const user = await this.get(id);
        user.sso_ticket = token;
        await user.save();
    }

    /**
     * Sets a new sso token to a user
     * @param id
     * @param token
     * @returns {Promise<void>}
     */
    static async setSex(id, sex) {
        const user = await this.get(id);
        if (sex === 'F') {
            user.figure = '5000160001630016950172501'
        } else {
            user.figure = '1000118001270012900121001'
        }
        user.sex = sex;
        await user.save();
        await Rcon.refreshLooks(id);
    }

    /**
     * Sets a new figure to a user
     * @param id
     * @param figure
     * @returns {Promise<void>}
     */
    static async setFigure(id, figure) {
        const user = await this.get(id);
        user.figure = figure;
        await user.save();
        await Rcon.refreshLooks(id);
    }

    /**
     * Gets a user password's
     * @param id
     * @returns {Promise<*>}
     */
    static async getPassword(id) {
        const user = await this.get(id);
        return user.password;
    }

    /**
     * Sets a new user password
     * @param id
     * @param password
     * @returns {Promise<void>}
     */
    static async setPassword(id, password) {
        const user = await this.get(id);
        user.password = password;
        await user.save();
    }

    /**
     * Checks if a username exists
     * @param username
     * @returns {Promise<boolean>}
     */
    static async exists(username) {
        return !!await this.getUserByName(username);
    }

    /**
     * Sets new credits amount to a user
     * @param id
     * @param amount
     * @returns {Promise<void>}
     */
    static async setCredits(id, amount) {
        amount = parseInt(amount);
        const user = await this.get(id);
        if (user.credits !== amount) {
            user.credits = amount;
            await user.save();
            Rcon.refreshCredits(id);
        }
    }

    static async setRank(id, rank) {
        const user = await this.get(id);
        user.rank = rank;
        await user.save();
    }

    static async setTickets(id, tickets) {
        const user = await this.get(id);
        user.tickets = tickets;
        await user.save();
    }

    static async setFilm(id, film) {
        const user = await this.get(id);
        user.film = film;
        await user.save();
    }

    /**
     * Sets new username to a user
     * @param id
     * @param username
     * @returns {Promise<void>}
     */
    static async setUsername(id, username) {
        const user = await this.get(id);
        user.username = username;
        await user.save();
    }

    /**
     * Sets a new console motto to a user
     * @param id
     * @param console_motto
     * @returns {Promise<void>}
     */
    static async setConsoleMotto(id, console_motto) {
        const user = await this.get(id);
        user.console_motto = console_motto;
        await user.save();
    }

    /**
     * Sets a new motto to a user
     * @param id
     * @param motto
     * @returns {Promise<void>}
     */
    static async setMotto(id, motto) {
        const user = await this.get(id);
        user.motto = motto;
        await user.save();
    }

    /**
     * Gives items to a user
     * @param id
     * @param {array} catalogueSaleCodes
     * @returns {Promise<void>}
     */
    static async giveItems(id, catalogueSaleCodes) {
        await asyncForEach(catalogueSaleCodes, async(sale_code) => {
            const catalogueItem = await CatalogueItem.findOne({
                where: { sale_code }
            })
            if (catalogueItem !== null) {
                await Item.create({
                    user_id: id,
                    definition_id: catalogueItem.definition_id,
                    custom_data: ""
                })
            }
        })
        await Rcon.refreshHand(id);
    }

    /**
     * Give items from a voucher code to a user
     * @param id
     * @param voucherItems
     * @returns {Promise<void>}
     */
    static async giveVoucherItems(id, voucherItems) {
        await asyncForEach(voucherItems, async(item) => {
            const catalogueItem = await CatalogueItem.findOne({
                where: { sale_code: item.catalogue_sale_code }
            })
            if (catalogueItem !== null) {
                await Item.create({
                    user_id: id,
                    definition_id: catalogueItem.definition_id,
                    custom_data: ""
                })
            }
        })
        await Rcon.refreshHand(id);
    }

    /**
     * Adds time to club subscription to a user
     * @param id
     * @param amount
     * @returns {Promise<void>}
     */
    static async addClubTime(id, amount) {
        const user = await this.get(id);
        user.club_expiration += amount;
        await user.save();
        Rcon.refreshClub(id);
        return user.club_expiration;
    }

    /**
     * Sets club subscription time to a user
     * @param id
     * @param amount
     * @returns {Promise<void>}
     */
    static async setClubTime(id, amount) {
        const user = await this.get(id);
        const now = Date.now() / 1000;
        user.club_expiration = now + amount;
        user.club_subscribed = now;
        user.club_gift_due = now;
        await user.save();
        Rcon.refreshClub(id);
        return user.club_expiration;
    }

    static async deleteHc(id) {
        const user = await this.get(id);
        user.club_expiration = 0;
        user.club_subscribed = 0;
        await user.save();
        Rcon.refreshClub(id);
    }

    /**
     * Gets latest connected users
     * @param limit
     * @returns {Promise<*>}
     */
    static async getLatestConnectedUsers(limit) {
        const users = await User.findAll({
            limit,
            order: [
                ['last_online', 'DESC']
            ]
        });
        return users.map(user => {
            return { id: user.id, username: user.username }
        })
    }

    /**
     * Gets a user by username
     * @param name
     * @returns {Promise<Model<any, any>>}
     */
    static async getUserByName(name) {
        return await User.findOne({
            where: sequelize.where(sequelize.fn('lower', sequelize.col('username')), name)
        });
    }

    /**
     * Gets a homepage formatted user
     * @param name
     * @returns {Promise<{snowstorm_points: *, badge: *, figure: *, rooms: *, battleball_points: *, motto: *, created_at: string, badge_active: *, friends: {figure: *, username: *}[], username: *}>}
     */
    static async getHomeUser(name) {
        const user = await this.getUserByName(name);
        const friends = await KeplerMessengerFriends.getFrom(user.id);
        const rooms = await KeplerRooms.getOwner(user.id);
        return {
            username: user.username,
            figure: user.figure,
            motto: user.motto,
            created_at: new Date(user.created_at).toLocaleDateString('fr', {
                year: 'numeric',
                month: 'short',
                day: 'numeric'
            }),
            badge: user.badge,
            badge_active: user.badge_active,
            battleball_points: user.battleball_points,
            snowstorm_points: user.snowstorm_points,
            friends,
            rooms
        }

    }

    /**
     * Checks if a user is banned
     * @param id
     * @returns {Promise<boolean>}
     */
    static async isBanned(id) {
        const ban = await UserBan.findOne({
            where: {
                ban_type: 'USER_ID',
                banned_value: id,
                banned_until: {
                    [Op.gt]: Date.now() / 1000
                }
            }
        });
        return ban !== null;
    }

    static async getBansList(id) {
        return await UserBan.findAll({
            where: {
                ban_type: 'USER_ID',
                banned_value: id
            },
            limit: 20
        })
    }

    /**
     * Saves a user
     * @param user
     * @returns {Promise<void>}
     */
    static async saveUser(user) {
        await user.save();
    }

    /**
     * Gets snowstorm best players
     * @param limit
     * @param offset
     * @returns {Promise<*>}
     */
    static async getSnowstormBestPlayerNames(limit, offset = 0) {
        const bestPlayers = await User.findAll({
            limit,
            order: [
                ['snowstorm_points', 'DESC']
            ]
        });
        return bestPlayers.map(player => {
            return { username: player.username, points: player.snowstorm_points }
        })
    }

    /**
     * Gets battleball best players
     * @param limit
     * @returns {Promise<*>}
     */
    static async getBattleballBestPlayerNames(limit) {
        const bestPlayers = await User.findAll({
            limit,
            order: [
                ['battleball_points', 'DESC']
            ]
        });
        return bestPlayers.map(player => {
            return { username: player.username, points: player.battleball_points }
        })
    }

    /**
     * Gets hc days left of a user
     * @param club_expiration
     * @returns {number}
     */
    static getHCDaysLeft(club_expiration) {
        if (club_expiration > 0) {
            if (club_expiration * 1000 > Date.now()) {
                const oneDay = 24 * 60 * 60 * 1000;
                const today = new Date(Date.now());
                const expirationDate = new Date(club_expiration * 1000);
                return Math.floor(Math.abs((today - expirationDate) / oneDay));
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Gets user infos
     * @param id
     * @returns {Promise<{figure: *, credits: *, hcDaysLeft: number, motto: *}>}
     */
    static async getInfos(id) {
        const user = await this.get(id);
        const hcDaysLeft = this.getHCDaysLeft(user.club_expiration);
        return {
            credits: user.credits,
            sex: user.sex,
            figure: user.figure,
            motto: user.motto,
            hcDaysLeft
        }
    }

    /**
     * Creates a user for model views
     * @param session
     * @returns {Promise<{figure: *, hcDaysLeft: number, credits: *, motto: *, avatar: string, username}|undefined>}
     */
    static async createHbsUser(session) {
        if (session.user) {
            const infos = await this.getInfos(session.user.id);
            const wedUser = await WedUser.findOne({
                where: { user_id: session.user.id }
            })
            return {
                username: session.user.username,
                hcDaysLeft: infos.hcDaysLeft,
                credits: infos.credits,
                motto: infos.motto,
                sex: infos.sex,
                avatar: '/habbo-imager?figure=' + infos.figure + '&reverse=true',
                figure: infos.figure,
                view: wedUser.view
            }
        } else {
            return undefined;
        }
    }

}

module.exports = KeplerUsers;