const models = require('../../models');
const KeplerUsers = require('./KeplerUsers');
const { RoomRight } = models;

class KeplerRoomRights {

    static async deleteRoomRights(room_id) {
        await RoomRight.destroy({
            where: {
                room_id
            }
        })
    }

    static async add(user_id, room_id) {
        await RoomRight.create({
            user_id,
            room_id
        })
    }

    static async remove(user_id, room_id) {
        await RoomRight.destroy({
            where: {
                user_id,
                room_id
            }
        })
    }

    static async checkRight(user_id, room_id) {
        const right = await RoomRight.findOne({
            where: {
                user_id,
                room_id
            }
        })
        return !!right;
    }

    static async get(room_id) {
        const rights = await RoomRight.findAll({
            where: {
                room_id
            }
        });
        const users = [];
        for (let i = 0; i < rights.length; i++) {
            const user = await KeplerUsers.get(rights[i].user_id);
            users.push(user);
        }
        return users;
    }

}

module.exports = KeplerRoomRights;