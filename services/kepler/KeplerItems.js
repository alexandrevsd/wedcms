const models = require('../../models');
const { Item, ItemDefinition, WedItemImage } = models;
const Rcon = require("../../services/Rcon");

class KeplerItems {

    static async count() {
        return await Item.count();
    }

    static async getItemDefinitions() {
        return await ItemDefinition.findAll();
    }

    static async get(id) {
        return await Item.findOne({ where: { id } });
    }

    //
    static async getOffset(offset = 0) {
        const item = await WedItemImage.findOne({
            offset,
            order: [
                ['id', 'ASC']
            ],
            where: {
                little_image: ''
            }
        })
        const itemDefinition = await ItemDefinition.findOne({
            where: { id: item.item_definition_id }
        })
        const newItem = {
            sprite: itemDefinition.sprite,
            name: itemDefinition.name,
            definition_id: item.item_definition_id,
            id: item.id
        }
        return newItem;
    }

    //temp
    static async editImage(imageId, little_image) {
        const image = await WedItemImage.findOne({
            where: { id: imageId }
        });
        image.little_image = little_image;
        await image.save();
    }

    static async giveItem(id, definition_id) {
        const definition = await KeplerItems.getItemDefinition(definition_id);
        let custom_data = undefined;
        if (definition.sprite === 'poster') {
            custom_data = definition.sprite_id;
        }
        await Item.create({
            user_id: id,
            definition_id: definition_id,
            custom_data
        });
        Rcon.refreshHand(id);
    }

    static async getItemDefinition(definitionId) {
        return await ItemDefinition.findOne({
            where: { id: definitionId }
        });
    }

    static async getItemImage(item_definition_id) {
        return await WedItemImage.findOne({
            where: { item_definition_id }
        });
    }

    static async getHand(offset = 0, limit = 20, user_id) {

        const countItems = await Item.count({
            limit,
            offset,
            order: [
                ['order_id', 'ASC']
            ],
            where: { user_id, room_id: 0 }
        })

        if (countItems === 0) {
            return { items: [], itemDefinitions: [], itemImages: [] };
        }

        const items = await Item.findAll({
            limit,
            offset,
            order: [
                ['order_id', 'ASC']
            ],
            where: { user_id, room_id: 0 }
        });

        const itemDefinitions = [];
        for (const item of items) {
            const itemDefinition = await this.getItemDefinition(item.definition_id);
            itemDefinitions.push(itemDefinition);
        }


        const itemImages = [];
        for (const itemDefinition of itemDefinitions) {
            itemImages.push(await this.getItemImage(itemDefinition.id));
        }

        return { items, itemDefinitions, itemImages };
    }



    static async getRoomItems(offset = 0, limit = 20, room_id) {

        const countItems = await Item.count({
            limit,
            offset,
            order: [
                ['order_id', 'ASC']
            ],
            where: { room_id }
        })

        if (countItems === 0) {
            return { items: [], itemDefinitions: [], itemImages: [] };
        }

        const items = await Item.findAll({
            limit,
            offset,
            order: [
                ['order_id', 'ASC']
            ],
            where: { room_id }
        });

        const itemDefinitions = [];
        for (const item of items) {
            const itemDefinition = await this.getItemDefinition(item.definition_id);
            itemDefinitions.push(itemDefinition);
        }


        const itemImages = [];
        for (const itemDefinition of itemDefinitions) {
            itemImages.push(await this.getItemImage(itemDefinition.id));
        }

        return { items, itemDefinitions, itemImages };
    }

    static async countHandItems(user_id) {
        return await Item.count({ where: { user_id, room_id: 0 } });
    }

    static async countRoomItems(room_id) {
        return await Item.count({ where: { room_id } });
    }

    static async deleteHand(user_id) {
        await Item.destroy({ where: { user_id, room_id: 0 } });
        Rcon.refreshHand(user_id);
    }

    static async deleteRoomItems(room_id) {
        await Item.destroy({ where: { room_id } });
    }

    static async delete(id) {
        const item = await KeplerItems.get(id);
        await Item.destroy({ where: { id } });
        Rcon.refreshHand(item.user_id);
        return item;
    }

}

module.exports = KeplerItems;