const { Op } = require('sequelize');
const models = require('../../models');
const { Room } = models;

class KeplerRooms {

    static async get(id) {
        return await Room.findOne({ where: { id } });
    }

    static async edit(editedRoom) {
        const room = await KeplerRooms.get(editedRoom.id);
        room.name = editedRoom.name;
        room.description = editedRoom.description;
        room.category = editedRoom.category;
        room.visitors_max = editedRoom.visitors_max;
        room.accesstype = editedRoom.accesstype;
        room.password = editedRoom.password;
        await room.save();
    }

    /**
     * Get rooms from owner id
     * @param owner_id
     * @returns {Promise<*>}
     */
    static async getOwner(owner_id) {
        const rooms = await Room.findAll({
            where: { owner_id }
        });
        return rooms.map(room => {
            return {
                id: room.id,
                name: room.name,
                description: room.description,
                accesstype: room.accesstype
            }
        })
    }

    static async getRooms(offset = 0, limit = 20, owner_id) {
        return await Room.findAll({
            limit,
            offset,
            order: [
                ['id', 'ASC']
            ],
            where: { owner_id }
        });
    }

    static async count() {
        return await Room.count({
            where: {
                [Op.not]: { owner_id: 0 }
            }
        })
    }

    static async countRooms(owner_id) {
        return await Room.count({ where: { owner_id } });
    }

    static async delete(id) {
        const room = await KeplerRooms.get(id);
        await Room.destroy({ where: { id } });
        return room;
    }

}

module.exports = KeplerRooms;