const models = require('../../models')
const {Setting} = models;

class KeplerSettings {

    /**
     * Get setting from db
     * @param setting
     * @returns {Promise<*>}
     */
    static async getSetting(setting) {
        const result = await Setting.findOne({
            where: {setting: setting}
        });
        return result.value;
    }

    /**
     * Gets hours needed to buy a rare
     * @param pageId
     * @returns {Promise<*>}
     */
    static async getHoursToBuyRare(pageId) {
        let result;
        const value = await this.getSetting('rare.cycle.pages');
        const pages = value.split('|');
        pages.forEach(page => {
            const values = page.split(',');
            const fetchPageId = values[0];
            const fetchTime = values[1];
            if(fetchPageId === pageId) {
                result = fetchTime;
            }
        });
        return result;
    }

}

module.exports = KeplerSettings;