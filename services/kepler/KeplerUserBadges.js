const models = require('../../models');
const { UserBadge } = models;

class KeplerUserBadges {

    static async get(user_id) {
        return await UserBadge.findAll({
            where: {
                user_id
            }
        })
    }

    static async deleteAll(user_id) {
        await UserBadge.destroy({
            where: {
                user_id
            }
        })
    }

    static async create(user_id, badge) {
        await UserBadge.create({
            user_id,
            badge
        })
    }

}

module.exports = KeplerUserBadges;