const models = require('../../models')
const {Voucher, VouchersItem} = models;

class KeplerVouchers {

    static async create(voucher) {
        for(let i = 0; i < voucher.items.length; i++) {
            await KeplerVouchers.createItem(voucher.voucher_code, voucher.items[i]);
        }
        return await Voucher.create({
            voucher_code: voucher.voucher_code,
            credits: voucher.credits,
            expiry_date: voucher.expiry_date,
            is_single_use: voucher.is_single_use,
        });
    }

    static async createItem(voucher_code, item) {
        return await VouchersItem.create({
            voucher_code,
            catalogue_sale_code: item.sale_code 
        })
    }

    static async delete(voucher_code) {
        const voucher = await KeplerVouchers.get(voucher_code);
        await KeplerVouchers.deleteItems(voucher_code);
        await Voucher.destroy({where: {voucher_code}});
        return voucher;
    }

    static async deleteItems(voucher_code) {
        await VouchersItem.destroy({where: {voucher_code}});
    }

    static async countItems(voucher_code) {
        return await VouchersItem.count({where: {voucher_code}});
    }

    static async getList(limit, page) {
        const vouchersCount = await this.count();
        let offset = page > 0 ? page * limit : 0;
        if(offset >= vouchersCount) {
            page = 0, offset = 0;
        }
        const vouchers = [];
        const vouchersFound = await Voucher.findAll({
            limit,
            offset,
            order: [['expiry_date', 'ASC']]
        });
        for(let i = 0; i < vouchersFound.length; i++) {
            const voucher = {
                voucher_code: vouchersFound[i].voucher_code,
                credits: vouchersFound[i].credits,
                expiry_date: vouchersFound[i].expiry_date,
                is_single_use: vouchersFound[i].is_single_use,
                furnis: await KeplerVouchers.countItems(vouchersFound[i].voucher_code)
            }
            vouchers.push(voucher);
        }
        const next = vouchersCount - (offset + limit) > 0;
        const previous = offset > 0;
        return {vouchers, page, next, previous};
    }

    static async count() {
        return await Voucher.count();
    }

    /**
     * Gets a voucher
     * @param voucher_code
     * @returns {Promise<*|boolean>}
     */
    static async get(voucher_code) {
        const voucher = await Voucher.findOne({
            where:{voucher_code}
        })
        return (voucher !== null) ? voucher : false;
    }

    /**
     * Checks if voucher is still active
     * @param voucher
     * @returns {boolean}
     */
    static isActive(voucher) {
        const voucher_expiry = new Date(voucher.expiry_date).getTime() / 1000;
        return Date.now() / 1000 <= voucher_expiry;
    }

    /**
     * Deletes a voucher if it is a single use one
     * @param voucher
     * @returns {Promise<void>}
     */
    static async deleteSingleUse(voucher) {
        if(voucher.is_single_use) {
            await voucher.delete();
        }
    }

    /**
     * Gets items (furnis) associated to a voucher
     * @param voucher
     * @returns {Promise<boolean|*>}
     */
    static async getVoucherItems(voucher) {
        const voucherItems = await VouchersItem.findAll({
            where:{voucher_code:voucher.voucher_code}
        })
        if(voucherItems === null) {
            return false;
        } else {
            return voucherItems.map(voucherItem => {
                return {
                    voucher_code: voucherItem.voucher_code,
                    catalogue_sale_code: voucherItem.catalogue_sale_code
                }
            });
        }

    }

}

module.exports = KeplerVouchers;