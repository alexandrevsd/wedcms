const models = require('../../models');
const { CataloguePackage } = models;
const { Op } = require("sequelize");

class KeplerCataloguePackages {

    static async get(salecode) {
        return await CataloguePackage.findOne({
            where: {
                salecode
            }
        })
    }

    static async delete(salecode) {
        return await CataloguePackage.destroy({
            where: {
                salecode
            }
        })
    }

    static async create(item) {
        return await CataloguePackage.create({
            salecode: item.sale_code,
            definition_id: item.definition_id,
            special_sprite_id: 0,
            amount: item.amount
        })
    }

}

module.exports = KeplerCataloguePackages;