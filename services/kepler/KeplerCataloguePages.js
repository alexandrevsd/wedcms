const models = require('../../models');
const { CataloguePage } = models;

class KeplerCataloguePages {

    static async create(pageInfos) {
        let newId = 1;
        const lastPage = await CataloguePage.findOne({
            order: [
                ['id', 'DESC']
            ]
        });
        if (lastPage !== null) newId = (lastPage.id + 1);
        let newOrderId = 0;
        const lastOrderPage = await CataloguePage.findOne({
            order: [
                ['order_id', 'DESC']
            ]
        });
        if (lastOrderPage !== null) newOrderId = (lastOrderPage.order_id + 1);
        let label_extra_s = '';;
        if (pageInfos.toggleInfoBubble) {
            label_extra_s = pageInfos.label_extra_s;
        }
        await CataloguePage.create({
            id: newId,
            name: pageInfos.name,
            name_index: pageInfos.name,
            order_id: newOrderId,
            min_role: pageInfos.min_role,
            image_headline: pageInfos.image_headline,
            is_club_only: pageInfos.is_club_only,
            index_visible: pageInfos.index_visible,
            body: pageInfos.body,
            label_extra_s,
            label_extra_t: pageInfos.label_extra_t,
            label_pick: pageInfos.label_pick,
            image_teasers: pageInfos.image_teasers,
            layout: pageInfos.layout
        })
    }

    static async delete(id) {
        await CataloguePage.destroy({
            where: {
                id
            }
        })
    }

    static async edit(pageInfos) {
        const page = await KeplerCataloguePages.get(pageInfos.id);
        page.name = pageInfos.name;
        page.min_role = pageInfos.min_role;
        page.image_headline = pageInfos.image_headline;
        page.is_club_only = pageInfos.is_club_only;
        page.index_visible = pageInfos.index_visible;
        page.body = pageInfos.body;
        if (pageInfos.toggleInfoBubble) {
            page.label_extra_s = pageInfos.label_extra_s;
        } else {
            page.label_extra_s = '';
        }
        page.label_extra_t = pageInfos.label_extra_t;
        page.label_pick = pageInfos.label_pick;
        page.image_teasers = pageInfos.image_teasers;
        await page.save();
    }

    static async get(id) {
        return await CataloguePage.findOne({
            where: {
                id
            }
        });
    }

    static async getAll() {
        return await CataloguePage.findAll({
            order: [
                ['order_id', 'ASC']
            ]
        });
    }

    static async editOrder(page_id, order_id) {
        const page = await KeplerCataloguePages.get(page_id);
        page.order_id = order_id;
        await page.save();
    }

}

module.exports = KeplerCataloguePages;