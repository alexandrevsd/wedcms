const models = require('../../models')
const {ItemDefinition, CatalogueItem} = models;

class KeplerRare {

    /**
     * Gets the actual catalogue rare
     * @returns {Promise<Model<any, TModelAttributes>>}
     */
    static async getActualRare() {
        return await CatalogueItem.findOne({
            where: {sale_code: 'wedrare'}
        });
    }

    /**
     * Gets an item definition from a catalogue item's definition_id
     * @param definition_id
     * @returns {Promise<Model<any, TModelAttributes>>}
     */
    static async fetchItemDefinition(definition_id) {
        return await ItemDefinition.findOne({
            where: {id: definition_id}
        })
    }

    /**
     * Gets actual catalogue rare infos
     * @returns {Promise<{imageName: *, price: *, name: *, description: *}>}
     */
    static async getCurrentRareInfos() {
        const rare = await this.getActualRare();
        const fetchedItemDefinition = await this.fetchItemDefinition(rare.definition_id);
        return {
            imageName: fetchedItemDefinition.sprite.replace('*', '-'),
            name: fetchedItemDefinition.name,
            description: fetchedItemDefinition.description,
            price: rare.price
        }
    }

}

module.exports = KeplerRare;