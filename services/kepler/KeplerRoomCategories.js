const { Op } = require('sequelize');
const models = require('../../models');
const { RoomCategory } = models;

class KeplerRoomCategories {

    static async get(minrole_setflatcat) {
        return await RoomCategory.findAll({
            where: {
                minrole_setflatcat: {
                    [Op.lte]: minrole_setflatcat
                }
            }
        })
    }

}

module.exports = KeplerRoomCategories;