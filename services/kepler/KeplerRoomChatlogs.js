const models = require('../../models');
const { RoomChatlog } = models;

class KeplerRoomChatlogs {

    static async deleteRoomChatlogs(room_id) {
        await RoomChatlog.destroy({
            where: {
                room_id
            }
        })
    }

    static async countRoomLogs(room_id) {
        return await RoomChatlog.count({
            where: {
                room_id
            }
        })
    }

    static async getRoomLogsList(offset, limit, room_id) {
        return await RoomChatlog.findAll({
            offset,
            limit,
            order: [
                ['timestamp', 'DESC']
            ],
            where: {
                room_id
            }
        })
    }

}

module.exports = KeplerRoomChatlogs;