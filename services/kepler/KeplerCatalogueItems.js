const models = require('../../models');
const KeplerItems = require('./KeplerItems');
const { CatalogueItem, CataloguePackage } = models;
const { Op } = require("sequelize");
const KeplerCataloguePackages = require('./KeplerCataloguePackages');

class KeplerCatalogueItems {

    static async get(id) {
        return await CatalogueItem.findOne({
            where: {
                id
            }
        })
    }

    static async editOrder(id, order_id) {
        const item = await KeplerCatalogueItems.get(id);
        item.order_id = order_id;
        await item.save();
    }

    static async delete(id) {
        return await CatalogueItem.destroy({
            where: {
                id
            }
        })
    }

    static async edit(newItem) {
        const item = await KeplerCatalogueItems.get(newItem.id);
        item.name = !newItem.is_package ? newItem.name : '';
        item.description = !newItem.is_package ? newItem.description : '';
        item.package_name = newItem.is_package ? newItem.name : '';
        item.package_description = newItem.is_package ? newItem.description : '';
        item.price = newItem.price;
        item.is_hidden = newItem.is_hidden;
        item.definition_id = newItem.definition_id;
        if (newItem.is_package) {
            let itemPackage;
            if (item.is_package) {
                itemPackage = await KeplerCataloguePackages.get(newItem.sale_code);
                itemPackage.definition_id = newItem.definition_id;
                itemPackage.amount = newItem.amount;
            } else {
                item.is_package = true;
                itemPackage = await KeplerCataloguePackages.create(newItem);
            }
            await itemPackage.save();
        } else {
            if (item.is_package) {
                item.is_package = false;
                await KeplerCataloguePackages.delete(newItem.sale_code);
            }
        }
        await item.save();
    }

    static async create(item) {
        await CatalogueItem.create({
            name: !item.is_package ? item.name : '',
            description: !item.is_package ? item.description : '',
            package_name: item.is_package ? item.name : '',
            package_description: item.is_package ? item.description : '',
            page_id: item.page_id,
            sale_code: item.sale_code,
            price: item.price,
            is_hidden: item.is_hidden,
            definition_id: item.definition_id,
            is_package: item.is_package
        });
        await KeplerCataloguePackages.create(item);
    }

    static async getItems() {
        return await CatalogueItem.findAll({
            where: {
                [Op.or]: [{
                        package_name: {
                            [Op.not]: null
                        }
                    },
                    {
                        name: {
                            [Op.not]: ''
                        }
                    }
                ]
            }
        });
    }

    static async getItemsFromPage(page_id) {
        return await CatalogueItem.findAll({
            where: {
                page_id
            },
            order: [
                ['order_id', 'ASC']
            ]
        });
    }

    static async getPackage(salecode) {
        return await CataloguePackage.findOne({
            where: {
                salecode
            }
        });
    }

}

module.exports = KeplerCatalogueItems;