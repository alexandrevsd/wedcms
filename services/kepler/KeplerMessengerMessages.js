const models = require('../../models');
const { MessengerMessage } = models;

class KeplerMessengerMessages {

    static async getAll(receiver_id) {
        return await MessengerMessage.findAll({
            where: {
                receiver_id
            }
        })
    }

}

module.exports = KeplerMessengerMessages;