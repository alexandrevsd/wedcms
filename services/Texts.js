const path = require("path");

class Texts {

    static getTextsFile() {
        return require(path.join(`../ressources/texts/wed_front.json`));
    }

    static get(text, layout, neededValue) {
        const texts = {
            global: text[layout]['global']
        };
        if(neededValue) {
            texts[neededValue] = text[layout]['pages'][neededValue];
        }
        return texts;
    }

}

module.exports = Texts;