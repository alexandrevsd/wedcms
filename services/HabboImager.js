const fs = require('fs');
const path = require('path');
const {createCanvas, loadImage} = require('canvas');

// TODO Créer un bras par sprite dans imagerdata propriété arm: 5 pour un bras de 5px de haut
// TODO Créer une méthode qui sors les sprites ainsi que ses couleurs par sexe pour le habbo imager sur le site


const ressourcesPath = './ressources/public/habbo-imager';

class HabboImager {

    // Basic figure : 1000118001265012850121001

    // Load all sprites
    static sprites = this.getAllSprites();

    static getFromDir(dir, map) {
        fs.readdirSync(ressourcesPath + '/M/' + dir).forEach(sprite => {
            if (sprite.includes('.png')) sprite = sprite.replace('.png', '');
            map.set(sprite, 'M');
        })
        fs.readdirSync(ressourcesPath + '/F/' + dir).forEach(sprite => {
            if (sprite.includes('.png')) sprite = sprite.replace('.png', '');
            map.set(sprite, 'F');
        })
    }

    static getAllSprites(sex = undefined) {
        let haircut;
        let face;
        let top;
        let pants;
        let shoes;
        if (sex !== undefined) {
            haircut = fs.readdirSync(ressourcesPath + '/' + sex + '/haircut');
            // Get all faces from M dir (faces are not dir but files)
            face = fs.readdirSync(ressourcesPath + '/' + sex + '/face').map((face) => face.replace('.png', ''));
            // Get all tops from M dir
            top = fs.readdirSync(ressourcesPath + '/' + sex + '/top');
            // Get all pants from M dir
            pants = fs.readdirSync(ressourcesPath + '/' + sex + '/pants');
            // Get all shoes from M dir
            shoes = fs.readdirSync(ressourcesPath + '/' + sex + '/shoes');
        } else {
            haircut = new Map();
            face = new Map();
            top = new Map();
            pants = new Map();
            shoes = new Map();
            this.getFromDir('haircut', haircut);
            this.getFromDir('face', face);
            this.getFromDir('top', top);
            this.getFromDir('pants', pants);
            this.getFromDir('shoes', shoes);
        }
        return {haircut, face, top, pants, shoes};
    }

    /**
     * Gets all the sprite's colors
     * @param sprites - What sprites we are looking for colors
     * @param sex - Sex of the figure
     * @returns {Map<int, array>}
     */
    static getAllSpritesColors(sprites, sex) {
        // Get all the old figure datas
        const oldfiguredata = require('.' + ressourcesPath + '/oldfiguredata.json')['colors'];
        // Get M figure datas
        const figureData = oldfiguredata[sex][0];
        // Create colors map
        const mappedColors = new Map();
        Object.entries(sprites).forEach(entry => {
            // type (haircut, face...)
            const type = entry[0];

            // Translate type to new type (haircut = hr, ...)
            let translatedType = 'hr';
            switch (type) {
                case 'face':
                    translatedType = 'hd';
                    break;
                case 'top':
                    translatedType = 'ch';
                    break;
                case 'pants':
                    translatedType = 'lg';
                    break;
                case 'shoes':
                    translatedType = 'sh';
                    break;
            }

            figureData[translatedType].forEach(oldSprite => {
                // Get all the colors for each sprite found in translated type
                const colors = oldSprite[0]['c'];
                const spriteId = oldSprite[0]['s'];
                // Save color for each sprite in colors map
                mappedColors.set(spriteId, colors);
            })
        })
        return mappedColors;
    }

    /**
     * Gets the Habbo image from figure (reversed)
     * @param {string} figure - Figure to generate
     * @returns {Promise<Buffer>}
     */
    static async getReverse(figure) {
        return await this.rotateImage(await this.createImage(this.decrypt(figure)));
    }

    /**
     * Gets the Habbo image from figure
     * @param {string} figure - Figure to generate
     * @returns {Promise<Buffer>}
     */
    static async get(figure) {
        return await this.createImage(this.decrypt(figure));
    }

    /**
     * Gets the Habbo image from figure
     * @param {string} figure - Figure to generate
     * @returns {Promise<Buffer>}
     */
    static async getBig(figure) {
        const canvas = createCanvas(128, 220);
        const context = canvas.getContext('2d');
        const image = await loadImage(await this.createImage(this.decrypt(figure)));
        context.drawImage(image, 0, 0, 128, 220);
        return canvas.toBuffer('image/png');
    }

    /**
     * Horizontal rotates an image
     * @param {Buffer} buffer - Buffer to horizontal rotate
     * @returns {Promise<Buffer>}
     */
    static async rotateImage(buffer) {

        // Create an image with the same size than a habbo image
        const canvas = createCanvas(64, 110);
        const context = canvas.getContext('2d');

        // Load generated habbo image
        const image = await loadImage(buffer);

        // Rotate image
        let {width, height} = image
        Object.assign(context.canvas, {width, height})
        context.translate(width, 0);
        context.scale(-1, 1);

        // Draw rotated image
        context.drawImage(image, 0, 0)

        // Return rotated png image
        return canvas.toBuffer('image/png');
    }

    /**
     * Creates image from figure parts
     * @param parts - Parts to show
     * @returns {Promise<Buffer>}
     */
    static async createImage(parts) {
        // Create 2D image
        const canvas = createCanvas(64, 110);
        const context = canvas.getContext('2d');

        // Draw habbo body with face color
        await this.drawBody(context, parts.get('face'));
        // Draw face
        await this.drawFace(context, parts.get('face'));
        // Draw shoes
        await this.drawShoes(context, parts.get('shoes'));
        // Draw pants
        await this.drawPants(context, parts.get('pants'));
        // Draw top
        await this.drawTop(context, parts.get('top'));
        // Draw haircut
        await this.drawHaircut(context, parts.get('haircut'));
        // Draw hand with face color
        await this.drawHand(context, parts.get('face'));

        // For some sprites (short t-shirts) we have to add an arm

        await this.drawArm(context, parts.get('face'), parts.get('top'));

        // Return png image
        return canvas.toBuffer('image/png');
    }

    /**
     * Draws body
     * @param context - Image to draw context
     * @param facePart - Part to
     * @returns {Promise<void>}
     */
    static async drawBody(context, facePart) {
        let bodyPath = 'normal';
        // For rounded faces, we have to set a different body
        switch (facePart.sprite) {
            case '185' :
            case '195' :
            case '200' :
            case '206' :
                bodyPath = 'plus';
                break;
        }
        // Load body image with face color
        const body = await loadImage(path.join(ressourcesPath + '/body/' + bodyPath + '/' + facePart.color + '.png'));
        context.drawImage(body, 0, 0, body.width, body.height);
    }

    /**
     * Draws face
     * @param context - Image to draw context
     * @param facePart - Part to draw
     * @returns {Promise<void>}
     */
    static async drawFace(context, facePart) {
        const face = await loadImage(path.join(ressourcesPath + '/' + this.sprites.face.get(facePart.sprite) + '/face/' + facePart.sprite + '.png'));
        // Get positioning informations
        const imagerData = require('.' + ressourcesPath + '/imagerdata.json')['face'][facePart.sprite];
        context.drawImage(face, imagerData.dX, imagerData.dY, face.width, face.height);
    }

    /**
     * Draws shoes
     * @param context - Image to draw context
     * @param shoesPart - Part to draw
     * @returns {Promise<void>}
     */
    static async drawShoes(context, shoesPart) {
        const shoes = await loadImage(path.join(ressourcesPath + '/' + this.sprites.shoes.get(shoesPart.sprite) + '/shoes/' + shoesPart.sprite + '/' + shoesPart.color + '.png'));
        // Get positioning informations
        const imagerData = require('.' + ressourcesPath + '/imagerdata.json')['shoes'][shoesPart.sprite];
        context.drawImage(shoes, imagerData.dX, imagerData.dY, shoes.width, shoes.height);
    }

    /**
     * Draws haircut
     * @param context - Image to draw context
     * @param haircutPart - Part to draw
     * @returns {Promise<void>}
     */
    static async drawHaircut(context, haircutPart) {
        const hair = await loadImage(path.join(ressourcesPath + '/' + this.sprites.haircut.get(haircutPart.sprite) + '/haircut/' + haircutPart.sprite + '/' + haircutPart.color + '.png'));
        // Get positioning informations
        const imagerData = require('.' + ressourcesPath + '/imagerdata.json')['haircut'][haircutPart.sprite];
        context.drawImage(hair, imagerData.dX, imagerData.dY, hair.width, hair.height);
    }

    /**
     * Draws pants
     * @param context - Image to draw context
     * @param pantsPart - Part to draw
     * @returns {Promise<void>}
     */
    static async drawPants(context, pantsPart) {
        const pants = await loadImage(path.join(ressourcesPath + '/' + this.sprites.pants.get(pantsPart.sprite) + '/pants/' + pantsPart.sprite + '/' + pantsPart.color + '.png'));
        // Get positioning informations
        const imagerData = require('.' + ressourcesPath + '/imagerdata.json')['pants'][pantsPart.sprite];
        context.drawImage(pants, imagerData.dX, imagerData.dY, pants.width, pants.height);
    }

    /**
     * Draws top
     * @param context - Image to draw context
     * @param topPart - Part to draw
     * @returns {Promise<void>}
     */
    static async drawTop(context, topPart) {
        const top = await loadImage(path.join(ressourcesPath + '/' + this.sprites.top.get(topPart.sprite) + '/top/' + topPart.sprite + '/' + topPart.color + '.png'));
        // Get positioning informations
        const imagerData = require('.' + ressourcesPath + '/imagerdata.json')['top'][topPart.sprite];
        context.drawImage(top, imagerData.dX, imagerData.dY, top.width, top.height);
    }

    /**
     * Draws arm
     * @param context - Image to draw context
     * @param facePart - Part to take color from
     * @param topPart - Top to get the arm information
     * @returns {Promise<void>}
     */
    static async drawArm(context, facePart, topPart) {
        // Load arm with face color
        const imagerData = require('.' + ressourcesPath + '/imagerdata.json')['top'][topPart.sprite];
        if (imagerData.arm !== undefined) {
            if(imagerData.arm > 0) {
                const arm_extend = await loadImage(path.join(ressourcesPath + '/body/arm_extend/' + facePart.color + '.png'));
                let positionY = 72;
                for (let i = 0; i < imagerData.arm; i++) {
                    context.drawImage(arm_extend, 38, positionY, arm_extend.width, arm_extend.height);
                    positionY--;
                }
            }
            const arm = await loadImage(path.join(ressourcesPath + '/body/arm/' + facePart.color + '.png'));
            context.drawImage(arm, 38, 73, arm.width, arm.height);
        }
    }

    /**
     * Draws hand
     * @param context - Image to draw context
     * @param facePart - Part to take color from
     * @returns {Promise<void>}
     */
    static async drawHand(context, facePart) {
        // Load hand with face color
        const hand = await loadImage(path.join(ressourcesPath + '/body/hand/' + facePart.color + '.png'));
        context.drawImage(hand, 37, 80, hand.width, hand.height);
    }

    /**
     * Decrypts figure to parts
     * @param figure
     * @returns {Map<string, {sprite, color}>}
     */
    static decrypt(figure) {
        // Slice figure each 5 chars
        const figureParts = figure.replace(/.{5}/g, '$&|').split('|');
        // Last part have to be deleted (empty)
        figureParts.pop();

        // Decode each code to know each part (haircut, face...)
        let parts = new Map();
        // For each 5 numbers combinations
        figureParts.forEach(figurePart => {
            // Sprite is the 3 first chars
            const sprite = figurePart.substr(0, 3);
            // Color is the 2 last chars
            const color = figurePart.substr(3, 2);

            // If sprite is in haircut's sprites, it's haircut part
            if (this.sprites.haircut.has(sprite)) {
                parts.set('haircut', {sprite, color});
                // If sprite is in face's sprites, it's haircut part
            } else if (this.sprites.face.has(sprite)) {
                parts.set('face', {sprite, color});
                // ...
            } else if (this.sprites.top.has(sprite)) {
                parts.set('top', {sprite, color});
                // ...
            } else if (this.sprites.pants.has(sprite)) {
                parts.set('pants', {sprite, color});
                // ...
            } else if (this.sprites.shoes.has(sprite)) {
                parts.set('shoes', {sprite, color});
            }
        })

        // If a part is missing, setting default sprite
        if (!parts.has('haircut')) {
            parts.set('haircut', {sprite: '100', color: '01'});
        }
        if (!parts.has('face')) {
            parts.set('face', {sprite: '180', color: '01'});
        }
        if (!parts.has('top')) {
            parts.set('top', {sprite: '210', color: '01'});
        }
        if (!parts.has('pants')) {
            parts.set('pants', {sprite: '270', color: '01'});
        }
        if (!parts.has('shoes')) {
            parts.set('shoes', {sprite: '290', color: '01'});
        }
        // Return sprites as parts (haircut, face, top, ...)
        return parts;
    }

}

module.exports = HabboImager;