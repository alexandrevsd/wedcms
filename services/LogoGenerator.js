const LETTERS_FOLDER = './ressources/logo-letters';

const {createCanvas, loadImage} = require('canvas');
const {asyncForEach} = require('../utils');
const fs = require('fs');
const path = require('path');

class LogoGenerator {

    static letters = new Map();

    /**
     * Get spacing for all letters
     * @param {int} policeFolder - Folder used for text generating
     * @returns {number} - Spacing
     */
    static getSpacing(policeFolder) {
        switch (policeFolder) {
            case 0:
                return -10;
            case 1:
                return -10;
            case 2:
                return -5;
            case 3:
                return -5;
        }
    }

    /**
     * Creates logo and save it
     * @param {string} text - Text to print
     * @param {int} policeFolder - Font to use
     */
    static async create(text, policeFolder = 0) {
        await this.loadLetters(policeFolder);

        // We will stock letters to print here
        const lettersImages = [];

        let width = 0;
        let height = 0;
        // For each letter in the text
        for (let i = 0; i < text.length; i++) {
            // Get the text letter
            let textLetter = text[i].toLowerCase();
            // Get the letter name (for catching spaces)
            let letter = textLetter === ' ' ? '+' : textLetter;
            // Get the image letter
            let image = this.letters.get(letter);
            // Add image width to the width of the final image and letter spacing if it's not the last letter
            width += (i < text.length - 1) ? image.width + this.getSpacing(policeFolder) : image.width;
            // Push the image for generating final image later
            lettersImages.push(image);
            height = image.height;
        }

        // Creating a canvas
        const logo = createCanvas(width, height);
        const context = logo.getContext('2d');

        // X position to place next letter
        let dx = 0;

        // For each letter image found
        lettersImages.forEach((image, index) => {
            // Draw letter on logo
            context.drawImage(image, dx, 0, image.width, height);
            // Add letter width to the next x position and letter spacing if it's not the last letter
            dx += (index < lettersImages.length - 1) ? image.width + this.getSpacing(policeFolder) : image.width;
        })

        // Generates a png image
        const buffer = logo.toBuffer('image/png');
        // Write image in public directory for display in app
        fs.writeFileSync('./ressources/public/images/logos/appLogo.png', buffer);
    }

    /**
     * Loads all letters of a font folder
     * @param {int} policeFolder - Folder to load
     */
    static loadLetters = async (policeFolder) => {
        // For each letter in letters folder
        await asyncForEach(fs.readdirSync(path.join(LETTERS_FOLDER + '/' + policeFolder)), async (letter) => {
            // We load the letter image
            const image = await loadImage(path.join(LETTERS_FOLDER + '/' + policeFolder + '/' + letter));
            // We stock the image
            this.letters.set(letter.replace('.gif', ''), image)
        })
    }


}

module.exports = LogoGenerator;