const models = require('../../models')
const {Sequelize} = require("sequelize");
const {WedNews: WedNewsModel} = models;
const {formatDate} = require('../../utils');
const texts = require('../../ressources/texts/wed_front.json');

class WedNews {

    /**
     * Gets a news by id
     * @param {int} id - Id to search
     * @returns {Promise<{date, author_rank: string, description: *, id: *, title: *, author_username: *, content: *}>}
     */
    static async getNews(id) {
        const news = await WedNewsModel.findOne({
           where: {id}
        })
        let rank = texts.backend.rank1;
        switch (news.author_rank) {
            case 2:
                rank = texts.backend.rank2
                break;
            case 3:
                rank = texts.backend.rank3
                break;
            case 4:
                rank = texts.backend.rank4
                break;
            case 5:
                rank = texts.backend.rank5
                break;
            case 6:
                rank = texts.backend.rank6
                break;
            case 7:
                rank = texts.backend.rank7
                break;
        }
        return {
            id: news.id,
            title: news.title,
            description: news.description,
            content: news.content,
            date: formatDate(news.date),
            author_username: news.author_username,
            author_rank: rank
        }
    }

    /**
     * Gets the latest news
     * @param limit - Limit to show
     * @returns {Promise<[]>}
     */
    static async getLatestNews(limit) {
        const news = await WedNewsModel.findAll({
            limit,
            order: [['id', 'DESC']]
        })
        const latestNews = [];
        news.forEach(oneNews => {
            latestNews.push({
                id: oneNews.id,
                title: oneNews.title,
                description: oneNews.description,
                date: formatDate(oneNews.date)
            })
        })
        return latestNews;
    }

    /**
     * Searches news
     * @param {string} query - Query words to search
     * @param {int} limit - Limit to show
     * @returns {Promise<[]>}
     */
    static async getSearchNews (query, limit) {
        const Op = Sequelize.Op;
        const news = await WedNewsModel.findAll({
            limit,
            where: {
                title: {
                    [Op.like]: '%' + query + '%'
                }
            },
            order: [['id', 'DESC']]
        })
        const latestNews = [];
        news.forEach(oneNews => {
            latestNews.push({
                id: oneNews.id,
                title: oneNews.title,
                description: oneNews.description,
                date: formatDate(oneNews.date)
            })
        })
        return latestNews;
    }

}

module.exports = WedNews;