const models = require('../../models');
const {WedPaypalPayment} = models;

class WedPaypalPayments {

    /**
     * Gets a Paypal payment
     * @param payment_id
     * @returns {Promise<{paypal_item_id: *, payment_id: *, id: *}>}
     */
    static async get(payment_id) {
        const paypalPayment = await WedPaypalPayment.findOne({
            where:{payment_id}
        })
        return {
            id: paypalPayment.id,
            payment_id: paypalPayment.payment_id,
            paypal_item_id: paypalPayment.paypal_item_id
        }
    }

    /**
     * Deletes a Paypal payment
     * @param payment_id
     * @returns {Promise<void>}
     */
    static async delete(payment_id) {
        const paypalPayment = await WedPaypalPayment.findOne({
            where:{payment_id}
        })
        paypalPayment.destroy();
    }

}

module.exports = WedPaypalPayments;