const models = require('../../models');
const {Op} = require("sequelize");
const {WedHelpMessage} = models;

class WedHelpMessages {

    static async create(values) {
        return await WedHelpMessage.create(values);
    }

    /**
     * Gets help message by ip adresse
     * @param {string} ip - IP to search
     * @returns {Promise<boolean>}
     */
    static async getIp(ip) {
        const helpMessage = await WedHelpMessage.findOne({
            where:{
                ip,
                date: {
                    [Op.gt]: new Date(Date.now() - 1000 * 60 * 5)
                }
            }
        })
        return !!helpMessage;
    }

}

module.exports = WedHelpMessages;