const models = require('../../models')
const {WedRank} = models;

class WedRanks {
    
    static async getRankNames() {
        return await WedRank.findAll();
    }

}

module.exports = WedRanks;