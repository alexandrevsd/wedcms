const models = require('../../models');
const {WedPaypalItem} = models;


class WedPaypalItems {

    /**
     * Gets all items
     * @returns {Promise<*>}
     */
    static async getAll() {
        const paypalItems = await WedPaypalItem.findAll({
            order: [['price', 'ASC']]
        });
        return paypalItems.map((item) => {
            return {
                id: item.id,
                price: item.price,
                credits: item.credits,
                currency: item.currency
            }
        })
    }

    /**
     * Gets one item by id
     * @param id
     * @returns {Promise<{credits: *, price: *, currency: *, id: *, paypal_currency: *}>}
     */
    static async get(id) {
        const paypalItem = await WedPaypalItem.findOne({
            where: {id}
        });
        return {
            id: paypalItem.id,
            price: paypalItem.price,
            paypal_currency: paypalItem.paypal_currency,
            currency: paypalItem.currency,
            credits: paypalItem.credits
        }
    }

}

module.exports = WedPaypalItems;