const models = require('../../models')
const {WedAnnouncement} = models;

class WedAnnouncements {

    /**
     * Gets 5 lasts announcements
     * @returns {Promise<[]>}
     */
    static async getLastAnnouncements() {
        const announcements = await WedAnnouncement.findAll({
            limit: 5,
            order: [['id', 'DESC']]
        })

        const lastAnnouncements = [];

        announcements.forEach(announcement => {
            const links = [];
            if(announcement.button1_text !== '') {
                links.push({
                    url: announcement.button1_link,
                    text: announcement.button1_text
                })
            }
            if(announcement.button2_text !== '') {
                links.push({
                    url: announcement.button2_link,
                    text: announcement.button2_text
                })
            }
            lastAnnouncements.push({
                title: announcement.title,
                content: announcement.content,
                image: announcement.image,
                links
            })
        });

        return lastAnnouncements;
    }

}

module.exports = WedAnnouncements;