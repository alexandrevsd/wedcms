const models = require('../../models')
const {WedUser} = models;

class WedUsers {

    static async create(values) {
        return await WedUser.create(values);
    }

    static async setView(user_id, view) {
        const wedUser = await WedUser.findOne({
            where:{user_id}
        });
        wedUser.view = view;
        await wedUser.save();
    }

}

module.exports = WedUsers;