const models = require('../../models');
const {WedSetting} = models;

class WedSettings {

    static async get(settingName) {
        return await WedSetting.findOne({
            where: {setting: settingName}
        });
    }

    static async getSetting(settingName) {
        const setting = await this.get(settingName);
        return {
            setting: setting.setting,
            value: setting.value
        }
    }

}

module.exports = WedSettings;