const WedPaypalPayments = require("./wed/WedPaypalPayments");
const WedPaypalItems = require("./wed/WedPaypalItems");
const paypal = require('paypal-rest-sdk');
const models = require('../models');
const {WedPaypalPayment} = models;

class Paypal {

    /**
     * Executes a callback if payment is verified
     * @param {Express.Request} req - Http request
     * @param callback
     */
    static async paymentVerification(req, callback) {
        // Payments informations from URL
        const paymentInfo = { paymentId: req.query.paymentId, PayerID: req.query.PayerID}
        // Payment registered in db to get buyed item
        const paypalPayment = await WedPaypalPayments.get(paymentInfo.paymentId);
        // Get buyed item
        const paypalItem = await WedPaypalItems.get(paypalPayment.paypal_item_id);
        // Defining payment to check
        const payment = {
            payer_id: paymentInfo.PayerID,
            transactions: [{
                amount: {
                    currency: paypalItem.paypal_currency,
                    total: `${paypalItem.price}`
                }
            }]
        }
        // Check payment and do callback (err if any error in payment, pay if payment is good)
        paypal.payment.execute(paymentInfo.paymentId, payment,{}, async (err, pay) => {
            await callback(err, pay, paypalItem, paymentInfo);
        })
    }

    /**
     * Creates a Paypal payment
     * @param {object} paypalItem - Paypal item to buy (price and content)
     * @param callback
     */
    static async createPayment(paypalItem, callback) {
        // Paypal configuration
        paypal.configure({
            mode: process.env.PAYPAL_MODE,
            client_id: process.env.PAYPAL_CLIENT_ID,
            client_secret: process.env.PAYPAL_CLIENT_SECRET
        });
        // Paypal payment declaration
        const create_payment_json = {
            'intent': 'sale',
            'payer': {
                'payment_method': 'paypal'
            },
            'redirect_urls': {
                'return_url': process.env.WEBSITE_URL + '/credits/success',
                'cancel_url': process.env.WEBSITE_URL + '/credits/paypal?error=true'
            },
            'transactions': [{
                'item_list': {
                    'items': [{
                        'name': `${paypalItem.credits} crédits`,
                        'sku': '1',
                        'price': `${paypalItem.price}`,
                        'currency': paypalItem.paypal_currency,
                        'quantity': 1
                    }]
                },
                'amount': {
                    'currency': paypalItem.paypal_currency,
                    'total': `${paypalItem.price}`
                },
                'description': 'Achat de crédits.'
            }]
        };
        // Create paypal payment
        paypal.payment.create(create_payment_json, async function (error, payment) {
            if (error) {
                throw error;
            } else {
                for (let link in payment.links) {
                    // Get approved url
                    if (payment.links[link].rel === 'approval_url') {
                        // Create db link to the paypal payment
                        await WedPaypalPayment.create({
                            payment_id: payment.id,
                            paypal_item_id: paypalItem.id
                        })
                        // Continue Controller work
                        callback(payment.links[link].href);
                    }
                }
            }
        });
    }

}

module.exports = Paypal;