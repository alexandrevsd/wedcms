const WedSettings = require("./wed/WedSettings");
const Texts = require("./Texts");

class App {

    name = '';
    text;

    constructor() {
        this.text = Texts.getTextsFile();
        this.loadSettings();
    }

    async loadSettings() {
        this.name = (await WedSettings.getSetting('name')).value;
    }

}

module.exports = App;