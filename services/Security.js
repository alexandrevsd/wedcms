class Security {

    /**
     * Checks if a user is logged
     * @param {Express.Request} req - Http request
     * @returns {boolean} - User is connected = true
     */
    static isLogged(req) {
        // If user got a session and user associated
        return req.session && req.session.user;
    }

    /**
     * Executes a callback if a user is connected
     * @param {Express.Request} req - Http request
     * @param {Express.Response} res - Http response
     * @param callback
     * @param app
     */
    static checkLogged(req, res, callback, app = {}) {
        // If logged, continue the controller work
        if (req.session && req.session.user) {
            callback(req, res, app);
        } else {
            // If not logged
            if (req.session && req.session.redirect) {
                // If get room from url: add room to redirect variable
                const room = req.session.room ? `&room=${req.session.room}` : '';
                // If get redirect from url: add redirect to redirect link and room if there is
                const redirect = req.session.redirect ? `?redirect=${req.session.redirect}${room}` : '';
                delete req.session.redirect;
                delete req.session.room;
                res.redirect(`/login${redirect}`);
            } else {
                res.redirect('/login')
            }
        }
    }

    static checkLoggedFromAPI(req, res, callback, app = {}) {
        // If logged, continue the controller work
        if (req.session && req.session.user) {
            callback(req, res, app);
        } else {
            res.json({
                response: 200,
                logged: false
            })
        }
    }

    /**
     * Executes a callback if user is not logged
     * @param {Express.Request} req - Http request
     * @param {Express.Response} res - Http response
     * @param callback
     */
    static checkNotLogged(req, res, app, callback) {
        // If user is not logged, continue controller work
        if (!req.session.user) {
            callback(req, res, app);
        } else {
            // If user not logged, redirect to home
            res.redirect('/')
        }
    }

    /**
     * Gets a SSO token
     * @returns {string} - Token
     */
    static getToken() {
        return require('crypto').randomBytes(48).toString('hex');
    }

}

module.exports = Security;