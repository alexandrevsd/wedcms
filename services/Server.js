const path = require('path');
const express = require('express');
const session = require('express-session');
const exphbs = require('express-handlebars');
const helpers = require('handlebars-helpers')();
require('./HandlebarsHelpers');
const bodyParser = require("body-parser");

class Server {

    #port;
    #app;
    #routes;
    #housekeepingRoutes;
    #APIRoutes;

    constructor() {
        this.#port = process.env.PORT || 80;
        this.#app = express();
        // Use sessions for express
        this.#app.use(session({
            secret: process.env.SECRET || 'secretbreachhere',
            resave: true,
            saveUninitialized: true
        }));
        this.#routes = require('../routes/routes');
        this.#APIRoutes = require('../routes/APIRoutes');
        this.#housekeepingRoutes = require('../routes/housekeepingRoutes');
    }

    /**
     * Generates Express middlewares
     * @private
     */
    #middlewares = () => {
        // Use template engine
        this.#templateEngine();
        // Use url vars
        this.#app.use(express.urlencoded({extended:false}))
        this.#app.use(bodyParser.urlencoded({ extended: true }));
        this.#app.use(bodyParser.json())
        // Set static public directory
        this.#app.use(express.static(path.join(__dirname, '../ressources/public')));
        // Declare routes
        this.#app.use(this.#APIRoutes);
        this.#app.use(this.#housekeepingRoutes);
        this.#app.use(this.#routes);
    }


    /**
     * Initiates Express server
     * @private
     */
    init() {
        this.#middlewares();
        this.#app.listen(this.#port, () => console.log(`Server listening at *:${this.#port}`))
    }


    /**
     * Generates templates engine
     * @private
     */
    #templateEngine = () => {
        // Declare template engine as Handlebars
        this.#app.engine('.hbs', exphbs({
            defaultLayout: 'main',
            extname: '.hbs',
            helpers: helpers
        }))
        this.#app.set('view engine', '.hbs');
    }

}

module.exports = Server;