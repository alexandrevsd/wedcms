const App = require("./App");
const Texts = require("./Texts");
const KeplerUsers = require("./kepler/KeplerUsers");

class Model {

    /**
     * Creates a model for all pages to show
     * @param {Express.Request} req - Http request
     * @param {string} layout - Layout to load
     * @param {string} pageTextsName - Text to load
     * @returns {Promise<{app: {name: string, language: string}, text: *, user: ({figure: *, hcDaysLeft: number|number, credits: *, motto: *, avatar: string, username: *}|any)}>}
     */
    static async create(req, app, pageTextsName, layout = 'main') {

        // Informations here will propagate in all views

        // Get all texts needed
        const text = (layout && pageTextsName) ? Texts.get(app.text, layout, pageTextsName) : undefined;
        // Get user if is connected
        const user = (req.session && req.session.user) ? await KeplerUsers.createHbsUser(req.session) : undefined;

        return {
            layout,
            app,
            text,
            user
        }
    }

}

module.exports = Model;