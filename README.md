# WedCMS

**WARNING ! WedCMS is in BETA for now !**

## Installation on Windows

**WARNING ! Only Windows steps are ready at the moment, Linux steps will come later...**

### Pre-requisites

For Kepler and WedCMS to work, you have to download and install some softwares :

 1. Download and install a web browser with Flash/Shockwave (like [Quackster's one](http://forum.ragezone.com/f353/portable-browser-flash-shockwave-basilisk-1192727/))
 2. Download and install [WAMP Server](https://www.wampserver.com/)
 3. Download and install [JDK 12+](https://www.oracle.com/java/technologies/downloads/#jdk17-windows)
 4. Download and install [NodeJS](https://nodejs.org/)

Note: You can replace WAMP Server by MariaDB (it's just for simply getting DB and PHPMyAdmin)

If choose WAMP Server, you can't use port 80 for WedCMS, this is for developement use only.

### Creating Database

 1. Run WAMP Server and go to [PHPMyAdmin](http://localhost/phpmyadmin/)
 2. Create a database named "kepler"
 3. Go into the database "kepler" and import the SQL file "wedcms_kepler.sql"
 
 You're done ! The database should be now created.

### Install WedCMS dependancies

 1. Open a command prompt and go (with "cd" command) into the WedCMS folder
 2. Type the command "npm install" and wait

WedCMS should be now installed.

### Configure Kepler

 1. Start Kepler server once by opening the file "Start Kepler Server.bat"
 2. Go into Kepler folder (wedcms/kepler) and open the file "server.ini"
 3. In this file, make sure that the "Database" part is correctly filled, if not just replace the values

Kepler should be now configured.

### Configure WedCMS

 1. Copy all the content of the file ".env.example" in the WedCMS folder
 2. Paste it in a new file named ".env" in the same folder
 3. Configure the file like you need

Here are some explanations on how to fill it :

    KEPLER_SERVER_HOSTNAME=#IP or Hostname of your server#
    KEPLER_SERVER_PORT=#Kepler server port in kepler/server.ini#
    KEPLER_MUS_PORT=#Kepler MUS port in kepler/server.ini#
    KEPLER_RCON_PORT=#Kepler RCON port in kepler/server.ini#
    PORT=#Port for the website (in general you want port 80 but not if you use WAMP)#
    LANGUAGE=#fr = French | en = English#
    MYSQL_URL=#mysql://username:password@localhost:3306/databasename MySQL credentials#
    SECRET=#Just place a random key here#
    WEBSITE_URL=#URL to the website (http://SERVER_HOSTNAME/)#
    RCON_ENCODING=#ascii is needed here for french letters#
    PAYPAL_CLIENT_ID=#Your paypal client ID#
    PAYPAL_CLIENT_SECRET=#Your paypal client secret#

## Start WedCMS and Kepler

 1. Start Kepler just by opening the file "Start Kepler Server.bat"
 2. Start WedCMS by typing "npm start" in a command prompt in the WedCMS folder

You should now be able to go on the website at your server's hostname and connect to your Server.